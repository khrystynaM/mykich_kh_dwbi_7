use Kh_Mykich_Libary
go


drop trigger if exists lib.tr_Authors1
go

create trigger  lib.tr_Authors1
on lib.Authors
after update
as
begin
--update

   
  insert into lib.Authors_log (

    Author_Id_new, 
	Name_new, 
    URL_new,
    Author_Id_old, 
    Name_old ,
    URL_old,
	operation_type)
    select 
     I.Author_Id, 
     I.Name,
     I.URL, 
     D.Author_Id, 
     D.Name, 
     D.URL,
    'U'
	     
   from inserted I left join deleted D on I.Author_Id = D.Author_Id
   end
   go