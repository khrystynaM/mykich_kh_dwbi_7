/*create new file group*/
use [master]
go

alter database [Kh_Mykich_Libary] ADD FILEGROUP [Data]
go

/*create new data file */
use [master]
go

alter database [Kh_Mykich_Libary] 
add file (NAME = N'Kh_Mykich_Libary_data', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Kh_Mykich_Libary_data.mdf' , SIZE = 5120KB , FILEGROWTH = 1024KB ) TO FILEGROUP [Data]
go

/*set default file group*/
use [Kh_Mykich_Libary]
go

IF NOT EXISTS (SELECT name FROM sys.filegroups WHERE is_default=1 AND name = N'Data') 
	alter database [Kh_Mykich_Libary] MODIFY FILEGROUP [Data] DEFAULT
GO

