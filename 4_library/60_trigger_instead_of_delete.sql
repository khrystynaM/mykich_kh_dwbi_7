USE Kh_Mykich_Libary
GO

drop trigger if exists lib.tr_Authors_log
go

-- trigger creation on table  Author_log that prevent deletion records in table Author_log
create trigger tr_Authors_log
on [lib].Authors_log 
instead of DELETE
as

BEGIN
   Rollback
   raiserror ('Deletions not allowed',16,1)
   end