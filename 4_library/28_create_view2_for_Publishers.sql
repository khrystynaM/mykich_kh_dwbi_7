use Kh_Mykich_Libary_view
go

drop view if exists [view].Publishers_view
go
--- create view2 (table Publishers)
create OR alter view [view].Publishers_view
(
	   [Publisher_Id]
      ,[Name]
      ,[URL]
      ,[inserted]
	  ,[inserted_by]
	  ,[updated]
	  ,[updated_by]
  )
  as
  select  [Publisher_Id]
      ,[Name]
      ,[URL]
      ,[inserted]
	  ,[inserted_by]
	  ,[updated]
	  ,[updated_by]

  from [Kh_Mykich_Libary].[lib].[Publishers]
  go

-- check our view
select *
from [view].Publishers_view