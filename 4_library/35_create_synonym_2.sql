use Kh_Mykich_Libary_synonym
go

drop synonym if exists syn.Authors_log
go

-- create synonym 2 for table Authors_log
create synonym syn.Authors_log for Kh_Mykich_Libary.lib.Authors_log
go

