use Kh_Mykich_Libary
go

-- simple select statement after data manipulation (after update records) to show results (table Authors)

select *
from [lib].Authors
go