use Kh_Mykich_Libary
go

-- update several records in the table Books


-- update record in the table Books, where ISBN = 900-6-323-014-1
UPDATE [lib].Books
SET 
	Price = '99'
	WHERE ISBN = '900-6-323-014-1'
GO
-- update record in the table Books, where ISBN = 908-9-386-014-2
UPDATE [lib].Books
SET 
	Price = '130'
	WHERE ISBN = '908-9-386-014-2'
GO

-- update record in the table Books, where URL = www.ladylib.net/avtora
UPDATE [lib].Books
SET 
	Price = '93'
	WHERE URL = 'www.ladylib.net/avtora'
GO
-- update record in the table Books, where Publisher_Id = 15
UPDATE [lib].Books
SET 
	URL = 'www.www.litmir.me/'
	WHERE Publisher_Id = 15
GO

-- update record in the table Books, where Publisher_Id = 23
UPDATE [lib].Books
SET 
	Price = '67'
	WHERE Publisher_Id = 23

-- update record in the table Books, where ISBN = 978-5-323-897-1
UPDATE [lib].Books
SET 
	Price = '90'
	WHERE ISBN = '978-5-323-897-1'
GO

-- update record in the table Books, where Publisher_Id = 20

UPDATE [lib].Books
SET 
	Price = '93'
	WHERE Publisher_Id = 20

GO

-- update record in the table Books, where ISBN = 973-3-247-464-8
UPDATE [lib].Books
SET 
	URL = 'www.phoenicis/author.com.ua'
	WHERE ISBN = '973-3-247-464-8'
GO

-- update record in the table Books, where ISBN = 973-0-211-014-2

UPDATE [lib].Books
SET 
	Price = '97'
	WHERE ISBN = '973-0-211-014-2'
GO

-- update record in the table Books, where ISBN = 978-5-323-897-1

UPDATE [lib].Books
SET 
	URL = 'www.tulu.org/a19290'
	WHERE ISBN = '978-5-323-897-1'
GO