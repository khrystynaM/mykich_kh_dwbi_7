use Kh_Mykich_Libary
go

-- simple select statement after data manipulation (after delete records) to show results (table Authors_log)
-- table Authors_log don't change
select *
from lib.Authors_log
go