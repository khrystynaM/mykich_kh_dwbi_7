use Kh_Mykich_Libary
go
-- delete rows from table Books

--delete row1
delete from lib.Books
where Publisher_Id = 25
go
--delete row2
delete from lib.Books
where Publisher_Id = 10
go

--delete row3
delete from lib.Books
where Publisher_Id = 5
go
--delete row4
delete from lib.Books
where Publisher_Id = 7
go
--delete row5
delete from lib.Books
where Publisher_Id = 9
go