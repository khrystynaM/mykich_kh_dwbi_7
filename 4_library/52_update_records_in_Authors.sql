use Kh_Mykich_Libary
go

-- update several records in the table Authors


-- update record in the table Authors, where Author_Id = 1
update lib.Authors
set 
	Name = 'V_Vynnychenko',
	URL = 'www.volodymyr_V.ua'
	where Author_Id = 1
go

-- update record in the table Authors, where Author_Id = 2
update [lib].Authors
set 
	URL = 'www.charlz_dickens.com'
	where Author_Id = 2
go

-- update record in the table Authors, where Author_Id = 5
update [lib].Authors
set 
	Name = 'Edgar_Poe',
	URL = 'www.edgar_poe.com'
	where Author_Id = 5
go
-- update record in the table Authors, where Name = Dariya_Kornij
update [lib].Authors
set 
	
	URL = 'www.dariya_kor.com'
	where Name = 'Dariya_Kornij'
go

-- update record in the table Authors, where Author_Id = 9
update [lib].Authors
set 
	Name = 'Stendhal'
	where Author_Id = 9

-- update record in the table Authors, where Author_Id = 21
update [lib].Authors
set 
	Name = 'P_Kulish',
	URL = 'www.pan_kulish.com'
	where Author_Id = 21
go

-- update record in the table Authors, where Author_Id = 10
update [lib].Authors
set 
	Name = 'O_Balzak',
	URL = 'www.onore_de_b.com'
	where Author_Id = 10
go

-- update record in the table Authors, where Name = Viktor_Gyugo
update [lib].Authors
set 
	URL = 'www.viktor_g.com.eu'
	where Name = 'Viktor_Gyugo'
go

-- update record in the table Authors, where URL = www.g_tkachenko.com

update [lib].Authors
set 
	Name = 'Hanna_Tkachenko'
	where URL = 'www.g_tkachenko.com'
go

-- update record in the table Authors, where Name = Emil_Zolya

update [lib].Authors
set 
	URL = 'www.em_Zolya.com'
	where Name = 'Emil_Zolya'
go