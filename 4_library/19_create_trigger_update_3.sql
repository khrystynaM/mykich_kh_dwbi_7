use Kh_Mykich_Libary
go


drop trigger if exists lib.tr_Books
go

---- trigger creation (on update) for table Books
create trigger tr_Books ON lib.Books
after update
as
begin
	update lib.Books
	set updated = getdate(), updated_by = SYSTEM_USER
	from lib.Books
		inner join inserted on lib.Books.ISBN = inserted.ISBN
end
go