use Kh_Mykich_Libary
go

-- simple select statement after data manipulation (after delete records) to show results (table Books)

select *
from lib.Books
go