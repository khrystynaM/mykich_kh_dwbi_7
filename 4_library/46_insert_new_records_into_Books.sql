use Kh_Mykich_Libary
go

-- insert new records into table Books


insert into lib.Books
(ISBN, Publisher_Id, URL, Price, updated, updated_by) values 
--row1
('966-5-386-014-2','6', 'www.rulit.me/books', '60', NULL,NULL),
--row2
('929-9-323-014-1','7', 'www.bukva.mobi/ldashvar', '55', NULL,NULL),
--row3
('988-1-113-094-7','8', 'www.read-online.in.ua', '78', NULL,NULL),
--row4
('973-0-211-014-2','9', 'www.dovidka.biz.ua', '100', NULL,NULL),
--row5
('978-4-217-414-5','10', 'www.librebook.me', '105', NULL,NULL),
--row6
('930-5-396-014-2','11', 'www.online-knigi.com', '59', NULL,NULL),
--row7
('900-6-323-014-1','12', 'www.alltxt.org.ua/klassika', '98', NULL,NULL),
--row8
('978-1-198-214-1','13', 'www.knigger.org/hugo/about', '117', NULL,NULL),
--row9
('979-4-291-014-2','14', 'www.biblioman.org/authors', '109', NULL,NULL),
--row10
('934-3-267-694-5','15', 'www.www.litmir.me', '134', NULL,NULL),
--row11
('908-9-386-014-2','16', 'www.moyaosvita.com.ua', '123', NULL,NULL),
--row12
('978-5-323-897-1','17', 'www.tululu.org/a19290', '97', NULL,NULL),
--row13
('934-6-173-314-1','18', 'www.poetyka.uazone.net/kobzar', '110', NULL,NULL),
--row14
('955-8-261-516-2','19', 'www.4itaem.com/author/lesya', '69', NULL,NULL),
--row15
('973-3-247-464-8','20', 'www.phoenicis.com.ua', '89', NULL,NULL),
--row16
('918-2-346-634-2','21', 'www.ukrclassic.com.ua', '121', NULL,NULL),
--row17
('909-8-329-014-1','22', 'www.ladylib.net/avtora', '99', NULL,NULL),
--row18
('922-7-193-318-7','23', 'www.bukva.mobi/volodimir-lis', '58', NULL,NULL),
--row19
('910-9-281-014-2','24', 'www.litmir.biz/rd/174733', '72', NULL,NULL),
--row20
('918-3-281-004-5','25', 'www.http://loveread.ec', '73', NULL,NULL)
go


