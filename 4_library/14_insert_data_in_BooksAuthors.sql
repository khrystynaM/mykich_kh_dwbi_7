use Kh_Mykich_Libary
go

-- insert data into table BooksAuthors


insert into lib.BooksAuthors
(BooksAuthors_Id, ISBN, Author_Id, updated, updated_by) values 
('1','978-5-386-014-2', '1', NULL, NULL),
--row2
('2', '978-5-323-014-1','2', NULL, NULL),
--row3
('3', '978-1-113-314-1','3', NULL, NULL),
--row4
('4', '978-1-211-014-2','4', NULL, NULL),
--row5
('5', '978-3-267-414-5','5', NULL, NULL)
go
