use Kh_Mykich_Libary_view
go

drop view if exists [view].BooksAuthors_view
go

--- create view4 (table BooksAuthors)
create or alter view [view].BooksAuthors_view
(
	   [BooksAuthors_Id]
	  ,[ISBN]
      ,[Author_Id]
      ,[Seq_No]
      ,[inserted]
	  ,[inserted_by]
	  ,[updated]
	  ,[updated_by]
  )
  as
  select   [BooksAuthors_Id]
	  ,[ISBN]
      ,[Author_Id]
      ,[Seq_No]
      ,[inserted]
	  ,[inserted_by]
	  ,[updated]
	  ,[updated_by]

  from [Kh_Mykich_Libary].[lib].[BooksAuthors]
  go

-- check our view

select *
from [view].BooksAuthors_view
go