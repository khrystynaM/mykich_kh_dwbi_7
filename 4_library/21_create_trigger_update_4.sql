use Kh_Mykich_Libary
go


drop trigger if exists lib.tr_BooksAuthors
go

---- trigger creation (on update) for table BooksAuthors
create trigger tr_BooksAuthors ON lib.BooksAuthors
after update
as
begin
	update lib.BooksAuthors
	set updated = getdate(), updated_by = SYSTEM_USER
	from lib.BooksAuthors
		inner join inserted on lib.BooksAuthors.BooksAuthors_Id = inserted.BooksAuthors_Id 
end
go
