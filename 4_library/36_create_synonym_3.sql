use Kh_Mykich_Libary_synonym
go

drop synonym if exists syn.Books
go

-- create synonym 3 for table Books
create synonym syn.Books for Kh_Mykich_Libary.lib.Books
go

