use Kh_Mykich_Libary
go

-- update several records in the table BooksAuthors


-- update record in the table BooksAuthors, where BooksAuthors_Id = 1
UPDATE [lib].BooksAuthors
SET 
	Seq_No= '2'
	WHERE BooksAuthors_Id = 1
GO
-- update record in the table BooksAuthors, where BooksAuthors_Id = 2
UPDATE [lib].BooksAuthors
SET 
	Seq_No= '2'
	WHERE BooksAuthors_Id = 2
GO

-- update record in the table BooksAuthors, where BooksAuthors_Id = 3
UPDATE [lib].BooksAuthors
SET 
	Seq_No= '3'
	WHERE BooksAuthors_Id = 3
GO
-- update record in the table BooksAuthors, BooksAuthors_Id = 10
UPDATE [lib].BooksAuthors
SET 
	Seq_No= '3'
	WHERE BooksAuthors_Id = 10
GO

-- update record in the table BooksAuthors, where BooksAuthors_Id = 9
UPDATE [lib].BooksAuthors
SET 
	Seq_No= '2'
	WHERE BooksAuthors_Id = 9
GO

-- update record in the table BooksAuthors, where BooksAuthors_Id = 12
UPDATE [lib].BooksAuthors
SET 
	Seq_No = '2'
	WHERE BooksAuthors_Id = 12
GO

-- update record in the table BooksAuthors, where BooksAuthors_Id = 15

UPDATE [lib].BooksAuthors
SET 
	Seq_No= '4'
	WHERE BooksAuthors_Id = 15
GO

-- update record in the table BooksAuthors, where BooksAuthors_Id = 17
UPDATE [lib].BooksAuthors
SET 
	Seq_No= '2'
	WHERE BooksAuthors_Id = 17
GO
-- update record in the table BooksAuthors, where BooksAuthors_Id = 22

UPDATE [lib].BooksAuthors
SET 
	Seq_No= '3'
	WHERE BooksAuthors_Id = 22
GO

-- update record in the table BooksAuthors, where BooksAuthors_Id = 25

UPDATE [lib].BooksAuthors
SET
	Seq_No = '3'
	WHERE BooksAuthors_Id = 25
GO