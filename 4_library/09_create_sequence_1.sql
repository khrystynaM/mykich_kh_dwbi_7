---- sequence creation
---- for table Authors for column Author_id

use Kh_Mykich_Libary
go

drop sequence if exists lib.seq_1
go


create sequence lib.seq_1 
as int
start with 1
increment by 1
go
