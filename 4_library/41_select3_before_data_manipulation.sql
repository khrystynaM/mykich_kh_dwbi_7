use Kh_Mykich_Libary
go

-- simple select statement before data manipulation to show results (table Books)
select *
from [lib].Books
go