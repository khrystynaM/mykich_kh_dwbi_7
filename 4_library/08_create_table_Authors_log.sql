use Kh_Mykich_Libary
go

drop table if exists lib.Authors_log
go



--- create table 5 - Authors_log
create table lib.Authors_log (
	   operation_id			int IDENTITY(1,1) NOT NULL PRIMARY KEY,	
	   Author_Id_new		int NULL,
	   Name_new				varchar(20) NULL,
	   URL_new  			varchar(30) NULL,
	   Author_Id_old		int NULL,
	   Name_old				varchar(20) NULL,
	   URL_old  			varchar(30) NULL,
	   operation_type		varchar(20) NOT NUll check (operation_type = 'I' or operation_type = 'U' or operation_type = 'D'), 
	   operation_datetime   datetime NOT NULL default(getdate()),

) on [Data]
go