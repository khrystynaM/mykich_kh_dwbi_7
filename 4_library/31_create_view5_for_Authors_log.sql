use Kh_Mykich_Libary_view
go

drop view if exists [view].Authors_log_view
go

--- create view5 (table Authors_log)
create or alter view [view].Authors_log_view
(
	   [operation_id]
	  ,[Author_Id_new]
      ,[Name_new]
      ,[URL_new]
      ,[Author_Id_old]
	  ,[Name_old]
	  ,[URL_old]
	  ,[operation_type]
	  ,[operation_datetime]
  )
  as
  select     [operation_id]
	  ,[Author_Id_new]
      ,[Name_new]
      ,[URL_new]
      ,[Author_Id_old]
	  ,[Name_old]
	  ,[URL_old]
	  ,[operation_type]
	  ,[operation_datetime]

  from [Kh_Mykich_Libary].[lib].[Authors_log]
  go

-- check our view
select *
from [view].Authors_log_view
go