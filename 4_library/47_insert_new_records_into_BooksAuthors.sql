use Kh_Mykich_Libary
go

-- insert new records into table BooksAuthors


insert into lib.BooksAuthors
(BooksAuthors_Id, ISBN, Author_Id, updated, updated_by) values 
--row1
('6','966-5-386-014-2', '6', NULL,NULL),
--row2
('7', '929-9-323-014-1','7', NULL,NULL),
--row3
('8', '988-1-113-094-7','8', NULL,NULL),
--row4
('9', '973-0-211-014-2','9', NULL,NULL),
--row5
('10', '978-4-217-414-5','10', NULL,NULL),
--row6
('11','930-5-396-014-2', '11', NULL,NULL),
--row7
('12', '900-6-323-014-1','12', NULL,NULL),
--row8
('13', '978-1-198-214-1','13', NULL,NULL),
--row9
('14', '979-4-291-014-2','14', NULL,NULL),
--row10
('15', '934-3-267-694-5','15', NULL,NULL),
--row11
('16','908-9-386-014-2', '16', NULL,NULL),
--row12
('17', '978-5-323-897-1','17', NULL,NULL),
--row13
('18', '934-6-173-314-1','18', NULL,NULL),
--row14
('19', '955-8-261-516-2','19', NULL,NULL),
--row15
('20', '973-3-247-464-8','20', NULL,NULL),
--row16
('21','918-2-346-634-2', '21', NULL,NULL),
--row17
('22', '909-8-329-014-1','22', NULL,NULL),
--row18
('23', '922-7-193-318-7','23', NULL,NULL),
--row19
('24', '910-9-281-014-2','24', NULL,NULL),
--row20
('25', '918-3-281-004-5','25', NULL,NULL)
go
