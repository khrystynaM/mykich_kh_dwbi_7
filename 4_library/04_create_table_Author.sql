use Kh_Mykich_Libary
go

drop table if exists [lib].Authors
go


--- create table 1 - Authors
CREATE TABLE [lib].Authors (
       Author_Id			int NOT NULL,
       Name					varchar(20) NOT NULL unique,
	   URL					varchar(30) NOT NULL  default('www.authorname.com'),
	   inserted				date NOT NULL default(getdate()),
	   inserted_by			varchar(30) NOT NULL default(system_user),
	   updated				date NULL, 
	   updated_by 			varchar(30) NULL

	  constraint PK_Authors primary key(Author_Id)
	
) on [Data]
go