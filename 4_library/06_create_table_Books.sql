use Kh_Mykich_Libary
go

drop table if exists lib.Books
go

--- create table 3 - Books
create table lib.Books (
       ISBN					varchar(15) NOT NULL,
	   Publisher_Id			int NOT NULL,
	   URL					varchar(30) NOT NULL,
	   Price				int NOT NULL default(0) check (Price >= 0),
	   inserted				date NOT NULL default(getdate()),
	   inserted_by			varchar(30) NOT NULL default(system_user),
	   updated				date NULL,
	   updated_by 			varchar(30) NULL

	  constraint PK_Books primary key(ISBN)
	  constraint FK_Books foreign key(Publisher_Id)
	  REFERENCES lib.Publishers (Publisher_Id) 
	on delete cascade
	on update cascade,
) on [Data]
go