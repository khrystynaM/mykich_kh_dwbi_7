use Kh_Mykich_Libary
go

drop table if exists lib.Publishers
go



--- create table 2 - Publishers
create table lib.Publishers (
       Publisher_Id			int NOT NULL,
       Name					varchar(20) NOT NULL unique,
	   URL					varchar(30) NOT NULL  default('www.publisher_name.com'),
	   inserted				date NOT NULL default(getdate()),
	   inserted_by			varchar(30) NOT NULL default(system_user),
	   updated				date NULL,
	   updated_by 			varchar(30) NULL

	  constraint PK_Publishers primary key(Publisher_Id)
	
) on [Data]
go