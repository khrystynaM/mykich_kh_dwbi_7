use Kh_Mykich_Libary
go

-- simple select statement after data manipulation (after delete records) to show results (table BooksAuthors)

select *
from lib.BooksAuthors
go