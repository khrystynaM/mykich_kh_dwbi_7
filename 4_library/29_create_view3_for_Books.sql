use Kh_Mykich_Libary_view
go

drop view if exists [view].Books_view
go

--- create view3 (table Books)
create or alter view [view].Books_view
(
	   [ISBN]
      ,[Publisher_Id]
      ,[URL]
	  ,[Price]
      ,[inserted]
	  ,[inserted_by]
	  ,[updated]
	  ,[updated_by]
  )
  as
  select  [ISBN]
      ,[Publisher_Id]
      ,[URL]
	  ,[Price]
      ,[inserted]
	  ,[inserted_by]
	  ,[updated]
	  ,[updated_by]

  from [Kh_Mykich_Libary].[lib].[Books]
  go

-- check our view
select *
from [view].Books_view
go