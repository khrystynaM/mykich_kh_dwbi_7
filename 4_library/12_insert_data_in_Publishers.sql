use Kh_Mykich_Libary
go

-- insert data into table Publishers
-- use sequense lib.seq_1 for column "Publisher_Id"


insert into lib.Publishers
(Publisher_Id, Name, URL,  updated, updated_by) values 
(NEXT VALUE FOR lib.seq_2,'Staryi_Lev', 'www.starylev.com.ua', NULL, NULL),
--row2
(NEXT VALUE FOR lib.seq_2,'Novyi_Svit_2000', 'www.novsv2000.com.ua', NULL, NULL),
--row3
(NEXT VALUE FOR lib.seq_2,'Svichado', 'www.svichado.com', NULL, NULL),
--row4
(NEXT VALUE FOR lib.seq_2,'Svit', 'www.http://svit.gov.ua', NULL, NULL),
--row5
(NEXT VALUE FOR lib.seq_2,'Kolves', 'www.kolves.lviv.ua', NULL, NULL)
go
