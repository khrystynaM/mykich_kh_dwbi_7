use Kh_Mykich_Libary
go

drop table if exists [lib].BooksAuthors
go

--- create table 4 - BooksAuthors

CREATE TABLE [lib].BooksAuthors(
  BooksAuthors_Id    int NOT NULL  default(1) check(BooksAuthors_Id>=1),
  ISBN               varchar(15) NOT NULL,
  Author_Id          int NOT NULl,
  Seq_No             int NOT NULL default(1) check(Seq_No>=1),
  inserted           date NOT NULL default(getdate()),
  inserted_by        varchar(30) NOT NULL default(system_user),
  updated            date NULL,
  updated_by         varchar(30) NULL,
  constraint UC_composite unique (ISBN, Author_Id),

  constraint PK_BooksAuthors primary key(BooksAuthors_Id),
  constraint FK_ISBN foreign key(ISBN) references lib.Books(ISBN) 
  on delete cascade
  on update cascade,
   
  constraint FK_AuthorId foreign key(Author_Id) references lib.Authors(Author_Id)
  on delete cascade
  on update cascade,

 
  ) ON [Data]
go