use Kh_Mykich_Libary
go


drop trigger if exists lib.tr_Publishers
go

---- trigger creation (on update) for table Publishers
create trigger tr_Publishers ON lib.Publishers
after update
as
begin
	update lib.Publishers
	set updated = getdate(), updated_by = SYSTEM_USER
	from lib.Publishers
		inner join inserted on lib.Publishers.Publisher_Id = inserted.Publisher_Id
end
go
