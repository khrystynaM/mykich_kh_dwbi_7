use Kh_Mykich_Libary
go

-- update several records in the table Publishers


-- update record in the table Publishers, where Publisher_Id = 1
UPDATE [lib].Publishers
SET 
	Name = 'Staryi_Lviv',
	URL = 'www.starui_lviv.com.ua'
	WHERE Publisher_Id = 1
GO
-- update record in the table Publishers, where Author_Id = 8
UPDATE [lib].Publishers
SET 
	Name = 'Proza_PC'
	WHERE Publisher_Id = 8
GO

-- update record in the table Publishers, where URL = www.kmbooks.com.ua
UPDATE [lib].Publishers
SET 
	Name = 'Buks'
	WHERE URL = 'www.kmbooks.com.ua'
GO
-- update record in the table Publishers, where Publisher_Id = 22
UPDATE [lib].Publishers
SET 
	Name = 'Logos_Ukraine'
	WHERE Publisher_Id = 22
GO

-- update record in the table Publishers, where Publisher_Id = 23
UPDATE [lib].Publishers
SET 
	Name = 'Green_pes'
	WHERE Publisher_Id = 23

-- update record in the table Publishers, where Publisher_Id = 24
UPDATE [lib].Publishers
SET 
	Name = 'Kalvariya'
	WHERE Publisher_Id = 24
GO

-- update record in the table Publishers, where Publisher_Id = 20

UPDATE [lib].Publishers
SET 
	Name = 'Pero_Press'
	WHERE Publisher_Id = 20

GO

-- update record in the table Publishers, where Name = Afisha
UPDATE [lib].Publishers
SET 
	URL = 'www.afisha_lviv.com.ua'
	WHERE Name = 'Afisha'
GO

-- update record in the table Publishers, where URL = www.osnova.com.ua

UPDATE [lib].Publishers
SET 
	Name = 'Osnova_Press'
	WHERE URL = 'www.osnova.com.ua'
GO

-- update record in the table Publishers, where Publisher_Id = 25

UPDATE [lib].Publishers
SET 
	Name = 'Ualangue_Press'
	WHERE Publisher_Id = 25
GO