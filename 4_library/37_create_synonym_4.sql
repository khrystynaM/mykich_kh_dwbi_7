use Kh_Mykich_Libary_synonym
go

drop synonym if exists syn.BooksAuthors
go

-- create synonym 4 for table BooksAuthors
create synonym syn.BooksAuthors for Kh_Mykich_Libary.lib.BooksAuthors
go

