use Kh_Mykich_Libary
go

-- insert data into table Authors
-- use sequense lib.seq_1 for column "Author_id"


insert into lib.Authors
(Author_id, Name, URL,  updated, updated_by) values 
(NEXT VALUE FOR lib.seq_1,'Dan_Browm', 'www.d_brown.com', NULL, NULL),
--row2
(NEXT VALUE FOR lib.seq_1,'Charles_Dickens', 'www.ch_dickens.com', NULL, NULL),
--row3
(NEXT VALUE FOR lib.seq_1,'Mark_Twain', 'www.m_twain.com', NULL, NULL),
--row4
(NEXT VALUE FOR lib.seq_1,'Jack_London', 'www.j_london.com', NULL, NULL),
--row5
(NEXT VALUE FOR lib.seq_1,'Edgar_Allan_Poe', 'www.e_poe.com', NULL, NULL)
go
