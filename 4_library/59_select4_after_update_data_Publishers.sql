use Kh_Mykich_Libary
go

-- simple select statement after data manipulation (after update records) to show results (table Publishers)

select *
from [lib].Publishers
go