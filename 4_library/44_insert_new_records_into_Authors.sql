use Kh_Mykich_Libary
go

-- insert new records into table Authors
-- use sequense lib.seq_1 for column "Author_id"


insert into lib.Authors
(Author_id, Name, URL,  updated, updated_by) values 

--row1
(NEXT VALUE FOR lib.seq_1,'Dariya_Kornij', 'www.d_kornij.com', NULL,NULL),
--row2
(NEXT VALUE FOR lib.seq_1,'Lyuko_Dashvar', 'www.l_dashvar.com', NULL,NULL),
--row3
(NEXT VALUE FOR lib.seq_1,'Vasyl_Shklyar', 'www.v_shklyar.com', NULL,NULL),
--row4
(NEXT VALUE FOR lib.seq_1,'Stendal', 'www.a_b_stendal', NULL,NULL),
--row5
(NEXT VALUE FOR lib.seq_1,'Onore_de_Balzak', 'www.o_balzak.com', NULL,NULL),
--row6
(NEXT VALUE FOR lib.seq_1,'Olena_Pechorna', 'www.o_pechorna.com', NULL,NULL),
--row7
(NEXT VALUE FOR lib.seq_1,'Emil_Zolya', 'www.e_zolya.com', NULL,NULL),
--row8
(NEXT VALUE FOR lib.seq_1,'Viktor_Gyugo', 'www.v_gyugo.com', NULL,NULL),
--row9
(NEXT VALUE FOR lib.seq_1,'Marsel_Prust', 'www.m_prust.com', NULL,NULL),
--row10
(NEXT VALUE FOR lib.seq_1,'Aleksandr_Dyuma', 'www.a_dyuma.com', NULL,NULL),
--row11
(NEXT VALUE FOR lib.seq_1,'Lev_Tolstoj', 'www.l_tolstoj.com', NULL,NULL),
--row12
(NEXT VALUE FOR lib.seq_1,'Vil`yam_Shekspir', 'www.v_shekspir.com', NULL,NULL),
--row13
(NEXT VALUE FOR lib.seq_1,'Taras_Shevchenko', 'www.t_shevchenko.com', NULL,NULL),
--row14
(NEXT VALUE FOR lib.seq_1,'Lesya_Ukrayinka', 'www.l_ukrayinka', NULL,NULL),
--row15
(NEXT VALUE FOR lib.seq_1,'Ivan_Franko', 'www.i_franko.com', NULL,NULL),
--row16
(NEXT VALUE FOR lib.seq_1,'Pantelejmon_Kulish', 'www.p_kulish.com', NULL,NULL),
--row17
(NEXT VALUE FOR lib.seq_1,'Margaret_Mitchell', 'www.m_mitchell.com', NULL,NULL),
--row18
(NEXT VALUE FOR lib.seq_1,'Volodymyr_Lys', 'www.vol_lys.com', NULL,NULL),
--row19
(NEXT VALUE FOR lib.seq_1,'Ganna_Tkachenko', 'www.g_tkachenko.com', NULL,NULL),
--row20
(NEXT VALUE FOR lib.seq_1,'Sergij_Zhadan', 'www.s_zhadan.com', NULL,NULL)
go
