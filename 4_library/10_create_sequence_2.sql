use Kh_Mykich_Libary
go

---- sequence creation
---- for table Publishers for column Publisher_Id

drop sequence if exists lib.seq_2
go


create sequence lib.seq_2 
as int
start with 1
increment by 1
go
