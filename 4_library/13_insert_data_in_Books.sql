use Kh_Mykich_Libary
go

-- insert data into table Books


insert into lib.Books
(ISBN, Publisher_Id, URL, Price, updated, updated_by) values 
('978-5-386-014-2','1', 'www.book-ye.com.ua', '120', NULL, NULL),
--row2
('978-5-323-014-1','2', 'www.ookclub.ua/ukr/', '105', NULL, NULL),
--row3
('978-1-113-314-1','3', 'www.booklya.ua/author/', '110', NULL, NULL),
--row4
('978-1-211-014-2','4', 'www.grenka.ua/155916/', '79', NULL, NULL),
--row5
('978-3-267-414-5','5', 'www.bukva.ua/catalog/', '210', NULL, NULL)
go
