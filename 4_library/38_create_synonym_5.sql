use Kh_Mykich_Libary_synonym
go

drop synonym if exists syn.Publishers
go

-- create synonym 4 for table Publishers
create synonym syn.Publishers for Kh_Mykich_Libary.lib.Publishers
go

