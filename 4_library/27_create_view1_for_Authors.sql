use Kh_Mykich_Libary_view
go

drop view if exists [view].Authors_view
go

--- create view1 (table Authors)
create OR alter view [view].Authors_view
(
	   [Author_Id]
      ,[Name]
      ,[URL]
      ,[inserted]
	  ,[inserted_by]
	  ,[updated]
	  ,[updated_by]
  )
	as
	select [Author_Id]
	  ,[Name]
	  ,[URL]
      ,[inserted]
	  ,[inserted_by]
	  ,[updated]
	  ,[updated_by]

  from [Kh_Mykich_Libary].[lib].[Authors]
  go


-- check our view 
 select *
 from [view].Authors_view