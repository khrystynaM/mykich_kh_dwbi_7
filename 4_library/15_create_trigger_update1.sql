use Kh_Mykich_Libary
go


drop trigger if exists lib.tr_Authors
go

---- trigger creation (on update) for table Authors
create trigger tr_Authors ON lib.Authors
AFTER UPDATE
AS
BEGIN
	update lib.Authors
	set updated = getdate(), updated_by = SYSTEM_USER
	from lib.Authors
		inner join inserted on lib.Authors.Author_Id = inserted.Author_Id
END
go

