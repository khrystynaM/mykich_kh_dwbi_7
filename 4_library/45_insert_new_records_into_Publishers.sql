use Kh_Mykich_Libary
go

-- insert new records into table Publishers
-- use sequense lib.seq_1 for column "Publisher_Id"


insert into lib.Publishers
(Publisher_Id, Name, URL,  updated, updated_by) values 

--row1
(NEXT VALUE FOR lib.seq_2,'Akta', 'www.acta.com.ua', NULL,NULL),
--row2
(NEXT VALUE FOR lib.seq_2,'Folio', 'www.folio.com.ua', NULL,NULL),
--row3
(NEXT VALUE FOR lib.seq_2,'Proza', 'www.pc-proza.net', NULL,NULL),
--row4
(NEXT VALUE FOR lib.seq_2,'Abetka', 'www.abetka.in.ua', NULL,NULL),
--row5
(NEXT VALUE FOR lib.seq_2,'Apriori', 'www.apriori.lviv.ua', NULL,NULL),
--row6
(NEXT VALUE FOR lib.seq_2,'Aspekt', 'www.aspekt.in.ua', NULL,NULL),
--row7
(NEXT VALUE FOR lib.seq_2,'Azymut_Ukrayina', 'www.azimut-ukraine.com', NULL,NULL),
--row8
(NEXT VALUE FOR lib.seq_2,'Znannya', 'www.znannia.com.ua', NULL,NULL),
--row9
(NEXT VALUE FOR lib.seq_2,'A-BA-BA-HA-LA-MA-HA', 'www.ababahalamaha.com.ua', NULL,NULL),
--row10
(NEXT VALUE FOR lib.seq_2,'KM-Buks', 'www.kmbooks.com.ua', NULL,NULL),
--row11
(NEXT VALUE FOR lib.seq_2,'Bukrek', 'www.bukrek.net', NULL,NULL),
--row12
(NEXT VALUE FOR lib.seq_2,'Fantaziya', 'www.fantaziya.com.ua', NULL,NULL),
--row3
(NEXT VALUE FOR lib.seq_2,'Afisha', 'www.afisha_l.ua', NULL,NULL),
--row14
(NEXT VALUE FOR lib.seq_2,'Osnova', 'www.osnova.com.ua', NULL,NULL),
--row15
(NEXT VALUE FOR lib.seq_2,'Pero', 'www.pero-press.com.ua', NULL,NULL),
--row16
(NEXT VALUE FOR lib.seq_2,'Bloomsbury', 'www.bloomsbury.com', NULL,NULL),
--row17
(NEXT VALUE FOR lib.seq_2,'Logos', 'www.logos-ukraine.com.ua', NULL,NULL),
--row18
(NEXT VALUE FOR lib.seq_2,'Zelenyj_pes', 'www.greenpes.com', NULL,NULL),
--row19
(NEXT VALUE FOR lib.seq_2,'Kal`variya', 'calvaria.org.ua', NULL,NULL),
--row20
(NEXT VALUE FOR lib.seq_2,'Ualangue', 'www.ualangue.com', NULL,NULL)
go
