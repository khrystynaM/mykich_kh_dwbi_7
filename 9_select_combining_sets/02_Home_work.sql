-- 1
-- �������� �������� ����� � ����� ��� 

use education
go

--create CTE
;with
categories_cte AS
(
select  name as category_name, id
from categories
where id between 1 and 3
) 
-- select data
select category_name
from categories_cte 
go

-- 2 
-- �������� �������� ����� � ����� ��� (� ������ � ��������� �� ������) 

;with
products_cte AS
(
select name as  name, city
from products 
),
products_London_cte AS
(
select name, city
from products_cte
where city = 'London'
)
-- select data
select name
from products_London_cte 
go

-- 3
-- �������� ����� (�� �Geography�) ������ ����� ������ ������� ���� (��������� �����,
-- ������� ������ ���������� ������)

use labor_sql
go


;with DirectReports(id, name, region_id, Place_level) as 
(
    select id, name, region_id, 1 
	from geography 
    where region_id = 1

	union all

	select g.id, g.name, g.region_id, Place_level + 1 
	from geography g
    inner join DirectReports d
        on g.region_id = d.id
)
select  region_id, id, name, Place_level
from DirectReports 
where region_id = 1
go

-- 4
-- �������� ����� (�� �Geography�) ���� ����� ������ ��� ����������� ������
-- (���������, �����-���������). ��������� �� ��������� ��������� ����� (������� ������
-- ���������� ������)

;with DirectReports(id, name, region_id, Place_level) as 
(
    select id, name, region_id, 1 
	from geography 
    where region_id = 4

	union all

	select g.id, g.name, g.region_id, Place_level + 1 
	from geography g
    INNER JOIN DirectReports d
        on g.region_id = d.id
)
select  region_id, id, name, Place_level
from DirectReports 

-- 5
-- ������� ����� ������� ������ ����������� ����� �� 1 �� 10 000.

;with recursive_cte (i) as
(
select 1
union all
select i+1 from recursive_cte where i < 10000
) 
select * from recursive_cte option (maxrecursion 0)
go

-- 6
-- ������� ����� ������� ������ ����������� ����� �� 1 �� 100 000.

;with recursive_cte (i) as
(
select 1
union all
select i+1 from recursive_cte where i < 100000
) 
select * from recursive_cte option (maxrecursion 0)
go

-- 7
-- ��������� ������� ������ ����� � ����� � ��������� ����.

declare @Year as int,
@FirstDateOfYear datetime,
@LastDateOfYear datetime

select @year = 2018
select @FirstDateOfYear = dateadd(yyyy, @Year, 0)
select @LastDateOfYear = dateadd(yyyy, @Year+ 1, 0)


;with cte as (
select 1 as DayID, @FirstDateOfYear AS FromDate,
datename(dw, @FirstDateOfYear) AS dayname

union all

select cte.DayID + 1 AS DayID,
dateadd(d, 1 ,cte.FromDate),
datename(dw, dateadd(d, 1 ,cte.FromDate)) AS dayname
from cte
where dateadd(d,1,cte.FromDate) < @LastDateOfYear
)
select  count(dayname) as amount_of_weekend_days
from CTE
where dayname IN ('Saturday','Sunday')
option   (maxrecursion 0)
go

-- 8
-- �� �����. �����. ������� ���������, �� ���������� ��, ��� �� �������� (����������� �������� IN).
-- ������� maker

select distinct maker
from product
where type in (select type from product where type = 'pc')
go

-- 9
-- �� �����. �����. ������� ���������, �� ����������  ��, ��� �� �������� 
--(����������� ������� ����� ALL). ������� maker.

select distinct maker
from product
where type <> all (select type from product where type != 'pc')
go

-- 10
-- �� �����. �����. ������� ���������, �� ���������� ��, ��� �� �������� 
-- (����������� ������� ����� ANY). ������� maker

select distinct maker
from product
where type = any (select type from product where type = 'pc')
go

-- 11
-- �� �����. �����. ������� ���������, �� ���������� ��������� �� �� �������� 
-- (����������� �������� IN). ������� maker

select distinct maker
from product
where type = 'pc' and 
maker in (select maker from product where type = 'laptop')
go

-- 12
-- �� �����. �����. ������� ���������, �� ���������� ��������� �� �� �������� 
-- (����������� ������� ����� ALL). ������� maker

select distinct maker
from product
where type = 'pc' and not
maker != all (select maker from product where type = 'laptop')
go

-- 13
-- �� �����. �����. ������� ���������, �� ���������� ��������� �� �� ��������
-- (����������� ������� ����� ANY). ������� maker

select distinct maker
from product
where type = 'pc' and 
maker = any (select maker from product where type = 'laptop')
go

-- 14
-- �� �����. �����. ������ ��� ��������� ��, �� ����� �� ���� � � �������� � ������� PC
-- (��������������� �������� �������� �� ��������� IN, ALL, ANY). ������� maker

select distinct maker
from product p1 where p1.type='pc' and
not exists
(select 1
from product p2
left join pc on p2.model=pc.model 
where p2.maker=p1.maker and 
p2.type='pc' and
pc.model is null)
go

-- 15
-- �� ������볻. ������� ����� ��� ������� ������ ('Ukraine').
-- ���� � �� ���� ����� ������� ������, ��� ������� ����� ��� ��� ������� � �� ����.
-- �������: country, class

select country,class 
from classes 
where class = ALL 
(select  class 
from classes 
where  country = 'Ukraine')
go

-- 16
-- �� ������볻. ������� ������, ���������� ��� ��������� ����, ����� ���, �� ���� ��������
-- � ���� � ����� ���� ('damaged'), � ���� (������ � ���) ����� ����� ������ � ������.
-- �������: ship, battle, date.

select out1.ship, out1.battle, bat1.date 
from outcomes out1
inner join battles bat1 on bat1.name = out1.battle
where out1.ship in 
(select ship from outcomes out2
inner join battles bat2 on bat2.name = out2.battle
where out2.result = 'damaged' and datediff( D,bat1.date,bat2.date) < 0)
and out1.result ='ok'
go

-- 17
-- �� �����. �����. ������ ��� ��������� ��, �� ����� �� ���� � � �������� � ������� PC 
-- (�������������� �������� EXISTS). ������� maker

select distinct maker
from product pr
where maker not in 
(select distinct maker
from product P
where P.type= 'pc' and not exists 
(select * from pc
where pc.model = p.model) )
go

-- 18
-- �� �����. �����. ������� ��������� ��������, �� ���������� �� � �������� �������� ���������.
-- �������: maker.

;with cte_maker_printer
as
(
select maker, max(speed) as max_speed
from pc inner join product on pc.model = product.model
where maker in
(
select distinct maker from product where type = 'printer'
and maker in (select distinct maker from product where [type] = 'pc')
)
group by maker
)

select maker from cte_maker_printer
where max_speed = (select MAX(max_speed)
from cte_maker_printer)
go

-- 19
-- �� ������볻. ������� ����� �������, � ���� ���� � ���� �������� ��� ���������� � ������.
-- �������: class. (����� ����� ������� ��������� �� �������� Ships, ���� ���� ��� ����,
-- ��� ���������� �� ���� ����� �� ������� � ������ �����, ����� �� � ��������)

select class 
from ships
where name in (select ship from outcomes  where result = 'sunk')

union

select class 
from classes
where class in (select ship from outcomes 
			where result = 'sunk')
go

-- 20
-- �� �����. �����. ������� ��������, �� ����� ������� ����. �������: model, price.

;with cte_max_price
as
(
select model, max(price) as price
from printer
group by model
)
select model, price
from cte_max_price
where price = (select max(price) from cte_max_price)
go

-- 21
-- �� �����. �����. ������� ��������, �������� ���� � ������ �� ��������
-- ����-����� � ��. �������: type, model, speed.

select p.type, l.model, l.speed
from laptop l
inner join product p on l.model = p.model
where speed < all (select speed from pc)
go

-- 22
-- �� �����. �����. ������� ��������� ����������� ���������� ��������.
-- �������: maker, price

;with cte_min_price
as
(
select  model, min(price) as price
from printer
where color = 'y'
group by model
)
select p.maker, cte.price
from cte_min_price cte
inner join product p on p.model = cte.model
where cte.price = (select min(price) from cte_min_price)
go


-- 23
-- �� ������볻. ������ �����, � ���� ����� ������ �� ������� �� ��� ������� ������ � 򳺿 � �����
-- (���� ����� ���������� ����� ������� Ships, � ����� ������� ��� ������� Outcomes,
-- �� ������� � ������� Ships, �� ����� �� �����). �������: ����� �����, �����, ������� �������.

select o.battle, c.country, count(o.ship) as ship_count
from outcomes o
left join ships s on s.name = o.ship
left join classes c on o.ship = c.class or s.class = c.class
where c.country is not null
group by c.country, o.battle
having count(o.ship) >= 2
go

-- 24
-- �� �����. �����. ��� ������� Product �������� ���������� ���� � ������ ������� � ��������� maker,
-- pc, laptop �� printer, � ��� ��� ������� ��������� ��������� ������� ������� ���������, �� ���
-- �����������, ����� ������ �������� ������� ��������� � ��������, ��������, PC, Laptop �� Printer.

select p.maker, count(pc.model) as pc, count(l.model) as laptop, count(p.model) as printer
from product p
left join laptop l on l.model = p.model
left join pc on pc.model = p.model
left join printer pr on pr.model = p.model
group by p.maker
go


-- 25
-- �� �����. �����. ��� ������� Product �������� ���������� ���� � ������ ������� � ��������� maker, pc,
-- � ��� ��� ������� ��������� ��������� �������, �� �������� �� ('yes'), �� �� ('no') ��������� ���
-- ���������. � ������� ������� ('yes') ��������� ������� ����� � ������� ������ �������� ������� ������
-- (�����, �� ����������� � ������� PC) ���������, ���������, 'yes(2)'.

-- 26
-- �� �Գ��� ����. ������������. ���������, �� ������ �� ������ ������ �� ������� ����� ������� ���������
-- �� ������ ������ ���� �� ���� (���� ������� Income_o �� Outcome_o), �������� ����� � ������ ���������
-- ������: point (�����), date (����), inc (������), out (������).

select i.point, i.date, i.inc, o.out
from income_o i
inner join outcome_o o on o.point = i.point and i.date = o.date
go

-- 27
-- �� ������볻. ��������� ����� ��� ������� � ������� Ships, �� �������������, � ��������� �������,
-- ��������� ����-���� �������� ������� � ���������� ������: numGuns=8, bore=15, displacement=32000,
-- type='bb', country='USA', launched=1915, class='Kongo'.4 �������: name, numGuns, bore, displacement, type,
-- country, launched, class.

-- 28
-- �� �Գ��� ����. ������������. ��������� ����� �� ����� ������ � �������� �� ������ ����� ������ �
-- ���������� �������� �� ���� ����� ������� � Outcome �� Outcome_o � �� ������ ����, ���� �����������
-- ������ �������� �������� ���� � �� ������ � ���. �������: ����� ������, ����, �����: � 'once a day', ����
-- ���� ������ � ������ � ����� � �������� ���� ��� �� ����; � 'more than once a day', ���� � � ����� �
-- �������� ������� ���� �� ����; � 'both', ���� ���� ������ � ���������

-- 29
-- �� �����. �����. ������� ������ ������� �� ���� ��� �������� (����-����� ����),
-- �� �������� ���������� 'B'. �������: maker, model, type, price.

select p.maker, p.model, p.type, l.price
from product p
inner join laptop l on l.model = p.model
where maker = 'B'

union all

select p.maker, p.model, p.type, pc.price
from product p
inner join pc  on pc.model = p.model
where maker = 'B'

union all

select p.maker, p.model, p.type, pr.price
from product p 
inner join printer pr on pr.model = p.model
where maker = 'B'
go


-- 30
-- �� ������볻. ����������� ����� �������� �������, �� � �������� � �� (��������� ����� � ������ � �������
-- Outcomes). �������: ����� �������, class.

select name, name as class from
(select name from ships
union
select ship from outcomes) s
inner join classes on s.name = classes.class
go

-- 31
-- �� ������볻. ������� �����, � ���� ������� ���� ���� �������� � �񳺿 ��
-- (��������� ����� ������ � ������� Outcomes, ���� ���� � ������� Ships). �������: class.

;with cte_count
as
(
	select class, name
	from ships
	
	union
	
	select classes.class, outcomes.ship
	from outcomes, classes 
	where outcomes.ship = classes.class
)

select class
from cte_count
group by class
having count(name)=1
go

-- 32
-- �� ������볻. ������� ����� ��� ������� � ��, ��� �� ����� ���������� �������,
-- �� ���� ���� ������� �� ���� �� 1942 �. �������: ����� �������.

select name
from ships
where launched < 1942

union

select ship
from outcomes
inner join battles on battles.name = outcomes.battle and DATEPART(year, battles.date) < 1942
go