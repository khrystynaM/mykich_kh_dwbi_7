use people_ua_db
go

-- index creation
create nonclustered index noncl_surname on person(surname)
with (fillfactor=50, pad_index=on)
go

create unique index unique_region_name on region([name])
with (fillfactor=20, pad_index=on)
go