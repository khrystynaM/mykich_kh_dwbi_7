use people_ua_db
go


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- create table for save result of fragmentation


create table [Table_def](
	[ID] [bigint] IDENTITY(794,1) NOT NULL,
	[db] [nvarchar](100) NULL,
	[shema] [nvarchar](100) NULL,
	[table] [nvarchar](100) NULL,
	[IndexName] [nvarchar](100) NULL,
	[frag_num] [int] NULL,
	[frag] [decimal](6, 2) NULL,
	[page] [int] NULL,
	[rec] [int] NULL,
    [func] [int] NULL,
	[ts] [datetime] NULL,
	[tf] [datetime] NULL,
	[frag_after] [decimal](6, 2) NULL,
	[object_id] [int] NULL,
	[idx] [int] NULL,
	[InsertUTCDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Defrag] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) )
GO

ALTER TABLE [Table_def] ADD  CONSTRAINT [DF_Defrag_InsertUTCDate]  DEFAULT (getutcdate()) FOR [InsertUTCDate];
GO


-- procedure for autoindex defragmentation

create procedure AutoDefragIndex
as
begin
	SET NOCOUNT ON;


	declare @IndexName nvarchar(100) 
	,@db nvarchar(100)			 
	,@Shema nvarchar(100)			 
	,@Table nvarchar(100)			
	,@SQL_Str nvarchar (2000)		
	,@frag decimal(6,2)				
	,@frag_after decimal(6,2)		  
    ,@frag_num int				 
	,@func int					
	,@page int					 	
	,@rec int						
	,@ts datetime					
	,@tf datetime					
    ,@object_id int					 
	,@idx int;						

	set @ts = getdate();
	
	
	select top 1
		@IndexName = index_name,
		@db=db,
		@Shema = shema,
		@Table = tb,
		@frag = frag,
		@frag_num = frag_num,
		@func=func,
		@page =[page],
		@rec = rec,
		@object_id = [object_id],
		@idx = idx 
	from  vIndexDefrag
	order by func*power((1.0-
	  convert(float,(select count(*) from [Table_def] vid where vid.db=db 
														 and vid.shema = shema
														 and vid.[table] = tb
														 and vid.IndexName = index_name))
	 /
	 convert(float,
                  case  when (exists (select top 1 1 from [Table_def] vid1 where vid1.db=db))
                            then (select count(*) from  [Table_def] vid1 where vid1.db=db)
                            else 1.0 end))
                    ,3) desc


	if(@db is not null)
	begin
	 

	 -- reorganization index

	   set @SQL_Str = 'alter index ['+@IndexName+'] on ['+@Shema+'].['+@Table+'] Reorganize';

		execute sp_executesql  @SQL_Str;

	
		set @tf = getdate()

	
		SELECT @frag_after = avg_fragmentation_in_percent
		FROM sys.dm_db_index_physical_stats
			(DB_ID(@db), @object_id, @idx, NULL , N'DETAILED')
		where index_level = 0;

	
		insert into [Table_def](
									[db],
									[shema],
									[table],
									[IndexName],
									[frag_num],
									[frag],
									[page],
									[rec],
									ts,
									tf,
									frag_after,
									object_id,
									idx
								  )
						select
									@db,
									@shema,
									@table,
									@IndexName,
									@frag_num,
									@frag,
									@page,
									@rec,
									@ts,
									@tf,
									@frag_after,
									@object_id,
									@idx;
		

		set @SQL_Str = 'UPDATE STATISTICS ['+@Shema+'].['+@Table+'] ['+@IndexName+']';

		execute sp_executesql  @SQL_Str;
	end
END

