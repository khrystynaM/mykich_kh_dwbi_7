use [people_ua_db]
go

-- generate random date


DECLARE @StartDate date = '1925-01-01',
		@EndDate date = '2000-01-01',
		@birthday date
DECLARE @counter int, @i int

set @counter = (select count(*) from person)
set @i = 1

while @i <= @counter
begin
	set @birthday = (select dateadd(day, rand(checksum(newid()))*(1+datediff(day, @StartDate, @EndDate)), @StartDate))
	
-- update table person

update person
set
birthday = @birthday
from person
where id_person = @i 
set @i = @i + 1

end
go


-- generate random region_id

update person
set
region_id = (ROUND(1+(RAND(CHECKSUM(NEWID()))*23),0))

go