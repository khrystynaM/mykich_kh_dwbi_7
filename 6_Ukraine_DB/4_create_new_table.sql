use [people_ua_db]
go

-- create table region of country
drop table if exists region
go

create table region(
region_id int identity(1,1),
name nvarchar(20),

constraint PK_region primary key(region_id))
go

-- create table of person

drop table if exists person
go

create table person(
id_person int identity(1,1),
surname int,
name int,
birthday date,
region_id int,

constraint PK_person primary key(id_person),
constraint FK_region foreign key(region_id) references region(region_id))
go

