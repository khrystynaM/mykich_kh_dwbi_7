use education
go

-- RUN STEP BY STEP IN ACCORDANCE WITH INSTRUCTIONS

-- first transaction
-- we select information about product, where productid = 1
begin transaction
select products.name, products.city, products.categoryid
from products
where productid = 1
go

-- after this, we run transation 2 (run script 2_transaction_2)


-- now, we want read information about product, where productid = 1 again
-- The query is not completed because the SELECT statement is blocked
select products.name, products.city, products.categoryid
from products
where productid = 1
go