use education
go

-- second transaction
-- we change products city where productid = 1
-- change will be successfully
use education
go

begin transaction
update products
set city = 'Kyiv'
where productid = 1
go


-- after this, we return to the transaction 1 (script 1_transaction_1)