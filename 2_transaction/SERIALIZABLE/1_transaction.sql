use education
go

-- RUN STEP BY STEP IN ACCORDANCE WITH INSTRUCTIONS

-- first transaction
-- we select information about product, where categoryid = 2
-- we see TWO ROW
set transaction isolation level serializable
begin transaction
select products.name, products.city, products.categoryid
from products
where categoryid = 2
go

-- now, go to the script 2 (2_insert_data)