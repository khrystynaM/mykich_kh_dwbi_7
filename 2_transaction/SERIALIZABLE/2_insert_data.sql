use education
go

-- insert new row and after this return to script 1 (1_transaction)
-- so, the query is wait, We can not insert new record when isolation level is serializable
insert into products
values (10, 'Reader2', 'Kyiv', 2)
go

-- when we run script 1 (1_transaction) we see the same result that was