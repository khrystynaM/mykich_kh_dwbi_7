use education
go

-- RUN STEP BY STEP IN ACCORDANCE WITH INSTRUCTIONS

-- we want change products city where categoryid = 3
-- the query is wait. Changes will not take place. We must cancell query
use education
go

update products
set city = 'Kyiv'
where categoryid = 3
go

-- now, we insert new row and after this return to script 1 (1_transaction)
insert into products
values (10, 'Reader2', 'Kyiv', 3)
go