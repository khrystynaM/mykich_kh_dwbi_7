use education
go

-- RUN STEP BY STEP IN ACCORDANCE WITH INSTRUCTIONS

-- first transaction
-- we select information about product, where categoryid = 3
-- we see ONE ROW
set transaction isolation level repeatable read
begin transaction
select products.name, products.city, products.categoryid
from products
where categoryid = 3
go

-- now, go to the script 2 (2_change_data)



-- after insert, we must run this script again
-- and we see TWO ROWS. So, the same transaction gives different results

select products.name, products.city, products.categoryid
from products
where categoryid = 3
go