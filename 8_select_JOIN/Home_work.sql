use labor_sql
go

-- 1
-- �� �����. �����. ������ ��������� ��� ��� ��, �� ����� �������� ���� �ᒺ��� �� �����
-- 8 �����. �������: maker, type, speed, hd.

select distinct product.maker, product.type, pc.speed, pc.hd
from pc 
inner join product on pc.model = product.model
where pc.hd <= 8
go

-- 2
-- �� �����. �����. ������� ��������� �� � ���������� 
-- �� ����� 600 ���. �������: maker

select  distinct product.maker
from product 
inner join pc on pc.model = product.model
where pc.speed >= 600
go

-- 3
--�� �����. �����. ������� ��������� �������� � ���������� �� ���� 500 ���. �������: maker.

select  distinct product.maker
from product 
inner join laptop on laptop.model = product.model
where laptop.speed <= 500
go

-- 4
-- �� �����. �����. ������� ���� ������� ��������, �� ����� ������� �ᒺ�� �������� ����� �� RAM 
--(������� Laptop). � ��������� ����� ���� ���������� ���� ���� ���.
-- ������� ���������: ������ � ������ �������, ������ � ������ �������, �ᒺ� ����� �� RAM.

--- ?????
select l1.model as model_larger, l2.model as model_lowest, l1.hd, l1.ram
from laptop l1
inner join laptop l2 on l1.ram = l2.ram and l1.hd = l2.hd
where l2.model < l1.model 
go


-- 5
-- �� ������볻. ������� �����, �� ���� ����� �� ��������� ������� ������� 'bb', ���
--  � ����� �������� 'bc'. �������: country, ���� � ������ 'bb', ���� � ������ 'bc'.

select c1.country, c1.type as type_bb,  c2.type as type_bc 
from classes c1
inner join classes c2 on c1.country = c2.country 
where c1.type < c2.type 

-- 6
-- �� �����. �����. ������� ����� ����� �� ��������� ��,
-- ��� �� ���� ����� �� 600 ���. �������: model, maker.

select  distinct product.maker, pc.model
from product 
inner join pc on pc.model = product.model
where pc.price < 600
go

-- 7
-- �� �����. �����. ������� ����� ����� �� ���������
--  �������, ��� �� ���� ���� �� 300 ���. �������: model, maker.

select  distinct product.maker, printer.model
from printer 
inner join product on product.model = product.model
where printer.price > 300
go

-- 8
-- �� �����. �����. ������� ���������, ����� ����� �� ����
-- ������� ���������, �� � � ��. �������: maker, model, price.

select product.maker, pc.model, pc.price
from product 
inner join pc on pc.model = product.model
go

-- 9
-- �� �����. �����. ������� �� ������ ����� ��, �� ��������� �� ����
--(���� ���� �������). �������: maker, model, price.

select distinct product.maker, product.model, pc.price
from product 
left join pc on pc.model = product.model
where product.type = 'pc'
go

-- 10
-- �� �����. �����. ������� ���������, ���, ������ �� ������� ��������� ��� ��������,
-- ������� ��������� ���� �������� 600 ���. �������: maker, type, model, speed.

select  distinct product.maker, product.type, product.model, laptop.speed
from product 
inner join laptop on laptop.model = product.model
where laptop.speed > 600
go

-- 11
-- �� ������볻. ��� ������� ������� Ships ������� �� ���������������

select ships.name, classes.displacement
from ships 
inner join classes on ships.class = classes.class
go

-- 12
-- �� ������볻. ��� �������, �� ������ � ������, �������
-- ����� �� ���� ����, � ���� ���� ����� ������

select battles.name, battles.date
from battles 
inner join outcomes on battles.name = outcomes.battle
where outcomes.result = 'OK'
go

-- 13
-- �� ������볻. ��� ������� ������� Ships ������� �����, ���� ���� ��������.

select ships.name, classes.country
from ships 
inner join classes on ships.class = classes.class
go

-- 14
-- �� ���������. ��� �������� ����� 'Boeing' ������� ����� ��������,
-- ���� ���� ��������.

select distinct company.name
from trip
inner join company on trip.id_comp = company.id_comp
where plane = 'Boeing'
go

-- 15
-- �� ���������. ��� �������� ������� Passenger �������
-- ����, ���� ���� ������������� ��������� �������.

select passenger.name, pass_in_trip.date
from passenger
inner join pass_in_trip on passenger.id_psg = pass_in_trip.id_psg
go

-- 16
-- �� �����. �����. ������ ������, ������� ��������� �� �ᒺ�
-- ��������� ����� ��� ��� ���������, �� �������������� �������������� 
-- 10 ��� 20 �� �� ������������ ���������� 'A'. �������: model, speed, hd. 
-- ������� ���� ������������ �� ���������� �� �������� speed.

select pc.model, pc.speed, pc.hd
from pc
right join product on pc.model = product.model
where (hd = 10 or hd = 20) and product.maker = 'A'
order by pc.speed asc
go

-- 17
-- ��� ������� ��������� � ������� Product ��������� �����
-- ������� ������� ���� ��������� (PIVOT).

select maker, [pc], [laptop], [printer]  
from product 
pivot
(count(model) for type  in ([pc], [laptop], [printer])) pvt 
go

-- 18
-- ���������� ������� ���� �� �������� � ��������� �� ������ ������. (PIVOT)

select  [avg_], [11], [12], [14], [15]
from
(select 'average price' as 'avg_', l.screen, l.price 
from laptop l) a
pivot
(avg(price) for screen in ([11], [12], [14], [15])) pvt
go

-- 19
-- ��� ������� �������� ��������� ������� ��'� ��������� (CROSS APPLY)

select product.maker, l.model, l.speed,l.ram,l.hd, l.price, l.screen
from product  
cross apply
(select * from laptop l where product.model= l.model) L
go

-- 20
-- ��� ������� �������� ��������� ������� ����������� ����
-- ����� �������� ���� � ���������. (CROSS APPLY)

select l.model, l.speed,l.ram,l.hd, l.price, l.screen, max_price
from laptop l
cross apply
(select max(price) max_price FROM Laptop l2
inner join  product p ON l2.model=p.model 
where maker = (select maker from product p2 where p2.model= l.model)) CA
go

-- 21
-- �'������ ����� ����� � ������� Laptop � ��������� ������ �
-- �������, �������� ����������� (model, code). (CROSS APPLY)

select *
from laptop l
cross apply
(select  top 1 *
from laptop l2 
where l.model < l2.model or (l.model = l2.model and l.code < l2.code) 
order by model, code) CA
order by l.model
go

-- 22
-- �'������ ����� ����� � ������� Laptop � ��������� ������ � �������,
-- �������� ����������� (model, code). (OUTER APPLY)

select *
from laptop l
outer apply
(select  top 1 *
from laptop l2 
where l.model < l2.model or (l.model = l2.model and l.code < l2.code) 
order by model, code) CA
order by l.model
go

-- 23
-- ������� � ������� Product �� ��� ����� � ���������� �������� � ����� �����,
-- ��� ��������������� ����� ���������. (CROOSS APPLY)

select p3.* 
from 
(select distinct type from product) p1 
cross apply
(select top 3 * from product p2 where  p1.type = p2.type order by p2.model) p3
go

-- 24
-- ��� ������� Laptop ����������� ���������� ��� �������� � ��� �������:
-- code, ����� �������������� (speed, ram, hd ��� screen), �������� �������������� .. (CROOSS APPLY)

select code, name, value
from laptop
cross apply
(values 
('speed', speed)
,('ram', ram)
,('hd', hd)
,('screen', screen)) table1 (name, value)
-- ���� ������� ���, �� �� �����, �� ������� ������ ����� where 
-- where code < 4 
go
