use triger_fk_cursor
go

insert into pharmacy (name, building_number, www, work_time, saturday, sunday, street)
values 
('Zelena_apteka', 23, 'www.zelena-apteka.com.ua', '12:00', 0, 0, 'Shevchenka'),
('D.S', 11, 'www.apteka-ds.com.ua', '00:00', 1, 1, 'Lazarenka'),
('Znakhar', 1, 'www.apteka-znahar.com.ua', '12:00', 1, 1, 'Gorodotska')
go
