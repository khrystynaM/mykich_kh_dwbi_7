use triger_fk_cursor
go

drop trigger if exists trigger_ministry_code_check
go

create trigger trigger_ministry_code_check on medicine
after insert, update
as
if exists
(
	select i.ministry_code FROM medicine
	inner join inserted i ON medicine.id = i.id
	WHERE i.ministry_code not like '[^MP][^MP]-[0-9][0-9][0-9]-[0-9][0-9]'
)
begin
	print 'Error!!! False ministry code: can not consist M,P letters'
	rollback transaction
end
go