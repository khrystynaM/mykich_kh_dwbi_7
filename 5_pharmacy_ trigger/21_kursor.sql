use triger_fk_cursor
go

declare @surname varchar(25), @name varchar(25)
declare @query_1 nvarchar(max), @query_2 nvarchar(max)
declare @number_column bit;

declare cursor1 CURSOR
for 
select surname, name 
from employee

open cursor1
	fetch next from cursor1 into @surname, @name
	while @@FETCH_STATUS = 0
	begin


		set @number_column = FLOOR(RAND()*(10-1+1)+1);
		set @query_1 = 'create table ' + @surname + @name + ' ('
		while @number_column > 0

		begin
			set @query_2 = @query_2 + 'column' + CONVERT(CHAR(1), @number_column) + ' INT, ';
			set @number_column = @number_column - 1;
		end

		set @query_2 = LEFT(@query_2, LEN(@query_2) - 1)
		set @query_2 = @query_2 + ')'
		EXEC(@query_2 + @query_2)
		fetch next from cursor1 into @surname, @name
	end
close cursor1
deallocate cursor1