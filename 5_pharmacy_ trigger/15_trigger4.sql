use triger_fk_cursor
go

drop trigger if exists trigger4_IU_f
go

create trigger trigger4_IU_f
on medicine_zone
after insert, update 
as 
if (select count(*) 
    from medicine, inserted 
    where medicine.id = inserted.medicine_id) !=
    @@rowcount 

  begin
    rollback transaction 
    print 'Error of updating and inserting!!!' 
  end  

if (select count(*) 
    from zone, inserted 
    where zone.id = inserted.zone_id) !=
    @@rowcount 

  begin
    rollback transaction 
    print 'Error of updating and inserting!!!' 
  end  
