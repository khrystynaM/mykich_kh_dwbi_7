use triger_fk_cursor
go

drop trigger if exists trigger3_IU_f
go

create trigger trigger3_IU_f
on pharmacy_medicine
after insert, update 
as 
if (select count(*) 
    from pharmacy, inserted 
    where pharmacy.id = inserted.pharmacy_id) !=
    @@rowcount 

  begin
    rollback transaction 
    print 'Error of updating or inserting!!!' 
  end 
   
if (select count(*) 
    from medicine, inserted 
    where medicine.id = inserted.medicine_id) !=
    @@rowcount 

  begin
    rollback transaction 
    print 'Error of updating or inserting!!!' 
  end 

else
 print 'Added!'
