use triger_fk_cursor
go

insert into medicine(name, ministry_code, recipe, narcotic, psychotropic)
values
('Korvalol', 'NO-50-002', 0, 0, 1),
('Izofluran', 'N0-11-006', 1, 0, 0),
('Dymedrol', 'R0-60-022', 1, 1, 1),
('Ibuprofen', 'X0-10-191', 0, 0, 0),
('Vitamin_A', 'AC-99-432', 0, 0, 0)
go