use triger_fk_cursor
go


-- create trigger on employee

drop trigger if exists trigger2_IU_f
go

create trigger trigger2_IU_f
on employee 
after insert, update
as 
if (select count(*) 
    from post, inserted 
    where post.post = inserted.post) !=
    @@rowcount 

  begin
    rollback transaction 
    print 'Error of inserting and updating' 
  end  

if (select count(*) 
    from pharmacy, inserted 
    where pharmacy.id = inserted.id) !=
    @@rowcount

  begin
    rollback transaction 
    print 'Error of inserting and updating' 
  end 

