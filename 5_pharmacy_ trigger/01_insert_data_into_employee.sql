use triger_fk_cursor
go

-- insert data into table employee
insert into dbo.employee ( 
						surname, 
						name,
						midle_name,
						identity_number,
						passport,
						experience,
						birthday,
						post,
						pharmacy_id)
values	
( 'Mykich','Khrystyna','Ihorivna', 1234808987,'CR09543', 3,'1991-11-17', 'pharmacist', 1),
( 'Matviiv','Ivanna','Vasylivna', 9730808081,'SN06100','1', '1997-01-10','assistant', 1),
( 'Ivaniv','Ihor','Stepanovych', 9661777098,'CT04219','9', '1978-09-21','boss', 1),
( 'Vasyliv','Petro','Ivanovych', 0349991688,'CK00889','2', '1978-09-21','assistant', 2),
( 'Petriv','Ruslana','Yaroslavivna', 7103210981,'CT11223','6', '1987-06-02','pharmacist', 2),
( 'Bojko','Inna','Ihorivna', 9555031925,'AB09182','11', '1964-12-12','boss', 2),
( 'Guschak','Marianna','Mykhailivna', 1187409821,'BA51909','4', '1994-09-25','pharmacist', 3),
( 'Vivcharchyn','Olena','Petrivna', 9510421678,'CN91106','2', '1992-11-08','assistant', 3),
( 'Stoiko','Anna','Gerasymivna', 4387109412,'CR01184','20', '1960-05-16','boss', 3)
go