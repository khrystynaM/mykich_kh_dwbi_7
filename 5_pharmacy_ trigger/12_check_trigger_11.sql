use triger_fk_cursor
go

-- insert data into table employee
-- new post - intern
insert into dbo.employee ( 
						surname, 
						name,
						midle_name,
						identity_number,
						passport,
						experience,
						birthday,
						post,
						pharmacy_id)
values	
( 'Fedevych','Olga','Ihorivna', 0930008933,'CM78213', 1,'1990-10-03', 'intern', 1)
go
-- select data from table post
-- there no post intern
select *
from post
go

-- update data
-- error
update employee
set
post = 'New_boss'
where id = 1