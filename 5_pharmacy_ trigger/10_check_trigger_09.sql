use triger_fk_cursor
go

-- insert data into table pharmacy
-- new street - Saharova
insert into pharmacy (name, building_number, www, work_time, saturday, sunday, street)
values 
('Zelena_apteka', 5, 'www.zelena-apteka.com.ua', '12:00', 0, 0, 'Saharova')
go

-- select data from table street
-- there no street Saharova
select *
from street
go
