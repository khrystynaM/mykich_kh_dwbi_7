use triger_fk_cursor
go

drop trigger if exists trigger_post_restriction
go

create trigger trigger_post_restriction ON post
after insert, update, delete
as
if exists
(
		select * from post
		inner join inserted i ON post.post = i.post
)
begin
	print 'Error!!! Modification of the data in the table is prohibited'
	rollback transaction
end