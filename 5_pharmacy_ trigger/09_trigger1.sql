use triger_fk_cursor
go

drop trigger if exists trigger1_IU_f
go

create trigger trigger1_IU_f
on pharmacy 
after insert, update
as 
if (select count(*) 
    from street, inserted 
    where street.street = inserted.street) !=
    @@rowcount 

  begin
    rollback transaction 
    print 'Error of updating and inserting' 
  end  

else
 print 'Added!'