use Kh_Mykich_Libary
go

drop trigger if exists Publishers_amount_D
go


create trigger Publishers_amount_D
on lib.Books
after DELETE
as

begin

update lib.Publishers 
set book_amount = (select count(lib.Books.Publisher_Id)
from lib.Books inner join deleted
on lib.Books.Publisher_Id = deleted.Publisher_Id), 

	issue_amount = (select sum(lib.Books.issue)
from lib.Books inner join deleted
on lib.Books.Publisher_Id = deleted.Publisher_Id),

	total_edition = (select sum(lib.Books.edition)
from lib.Books inner join deleted
on lib.Books.Publisher_Id = deleted.Publisher_Id)
where lib.Publishers.Publisher_Id IN (select Publisher_Id from deleted)
end

go