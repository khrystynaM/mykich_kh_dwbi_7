use Kh_Mykich_Libary
go

-- insert data into table BooksAuthors



insert into lib.BooksAuthors
(BooksAuthors_Id, ISBN, Author_Id, updated, updated_by) values 

-- row 26
('26','978-90-39-31-5', '26', NULL, NULL),
-- row 27
('27', '966-81-82-91-5','27', NULL, NULL),
-- row 28
('28', '271-90-11-29-6','28', NULL, NULL),
-- row 29
('29', '271-90-11-29-6','29', NULL, NULL),
-- row 30
('30', '390-21-70-34-4','30', NULL, NULL),
-- row 31
('31','142-09-29-81-9', '30', NULL, NULL),
-- row 32
('32', '121-06-25-09-9','31', NULL, NULL),
-- row 33
('33', '431-11-01-33-4','32', NULL, NULL),
-- row 34
('34', '898-66-23-01-0','33', NULL, NULL),
-- row 35
('35', '222-00-92-67-7','33', NULL, NULL),
-- row 36
('36','399-11-06-98-1', '33', NULL, NULL),
-- row 37
('37', '198-44-01-17-3','34', NULL, NULL),
-- row 38
('38', '091-13-19-24-15','36', NULL, NULL),
-- row 39
('39', '091-13-19-24-15','37', NULL, NULL),
-- row 40
('40', '271-90-11-29-6','44', NULL, NULL),
-- row 41
('41','314-99-91-22-7', '38', NULL, NULL),
-- row 42
('42', '093-11-17-77-8','45', NULL, NULL),
-- row 43
('43', '871-16-55-12-8','45', NULL, NULL),
-- row 44
('44', '231-10-01-09-8','43', NULL, NULL),
-- row 45
('45', '777-17-23-01-0','39', NULL, NULL),
-- row 46
('46', '777-17-23-01-0','40', NULL, NULL),
-- row 47
('47', '201-10-01-09-8','41', NULL, NULL),
-- row 48
('48', '299-13-17-98-3','35', NULL, NULL),
-- row 49
('49', '202-00-94-67-9','35', NULL, NULL),
-- row 50
('50', '222-00-92-67-7','41', NULL, NULL)
go

