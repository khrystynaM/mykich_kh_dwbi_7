use Kh_Mykich_Libary
go

alter table lib.Authors_log
add
book_amount_old int,
issue_amount_old int,
total_edition_old int,
book_amount_new int,
issue_amount_new int,
total_edition_new int
go