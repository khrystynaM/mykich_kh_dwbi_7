use Kh_Mykich_Libary
go

drop trigger if exists Publishers_amount_U
go

create trigger Publishers_amount_U
on lib.Books
after UPDATE
as 
BEGIN
if update (ISBN) or update (Publisher_Id)
or update (URL) or update (Price)
or update (title) or update (edition)
or update (published) or update (issue)
update lib.Publishers 
set book_amount = (select count(lib.Books.Publisher_Id)
from lib.Books inner join inserted
on lib.Books.Publisher_Id = inserted.Publisher_Id), 

	issue_amount = (select sum(lib.Books.issue)
from lib.Books inner join inserted
on lib.Books.Publisher_Id = inserted.Publisher_Id),

	total_edition = (select sum(lib.Books.edition)
from lib.Books inner join inserted
on lib.Books.Publisher_Id = inserted.Publisher_Id)
where lib.Publishers.Publisher_Id IN (select Publisher_Id from inserted)
end


go