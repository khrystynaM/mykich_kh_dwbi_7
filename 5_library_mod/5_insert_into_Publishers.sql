use Kh_Mykich_Libary
go

-- insert data into table Publishers
-- use sequense lib.seq_1 for column "Publisher_Id"


insert into lib.Publishers
(Publisher_Id, Name, URL, country, city, updated, updated_by) values 

--row1
(NEXT VALUE FOR lib.seq_2,'Dobre_sertse',  'www.dobreserce.in.ua', 'Ukraine', 'Drogobych', NULL, NULL),
--row2
(NEXT VALUE FOR lib.seq_2,'Duliby', 'www.duliby.com.ua', 'Ukraine', 'Kyiv', NULL, NULL),
--row3
(NEXT VALUE FOR lib.seq_2,'Geneza', 'www.geneza.ua', 'Ukraine', 'Kyiv', NULL, NULL),
--row4
(NEXT VALUE FOR lib.seq_2,'Ezdra', 'www.ezdra.com', 'Ukraine', 'Oleksandriya', NULL, NULL),
--row5
(NEXT VALUE FOR lib.seq_2,'Tao', 'www.duliby.com.ua', 'Ukraine', 'Kyiv', NULL, NULL),
--row6
(NEXT VALUE FOR lib.seq_2,'CLE_international',  'www.cle-international.com', 'French', 'Paris', NULL, NULL),
--row7
(NEXT VALUE FOR lib.seq_2,'Dider', 'www.editionsdidier.com', 'French', 'Paris', NULL, NULL),
--row8
(NEXT VALUE FOR lib.seq_2,'Ukrpol', 'www.ukrpol.ua/?', 'Ukraine', 'Stryi', NULL, NULL),
--row9
(NEXT VALUE FOR lib.seq_2,'MM_Publications', 'www.mmpublications.com', default, default, NULL, NULL),
--row10
(NEXT VALUE FOR lib.seq_2,'Vesna', 'www.vesna-books.com.ua', 'Ukraine', 'Kharkiv', NULL, NULL)
go




