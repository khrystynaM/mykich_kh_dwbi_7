use Kh_Mykich_Libary
go

alter table lib.Authors
add 
birthday date null,
book_amount int not null default 0, check (book_amount >= 0),
issue_amount int not null default 0,check (issue_amount >= 0),
total_edition int not null default 0, check(total_edition >= 0) 
go