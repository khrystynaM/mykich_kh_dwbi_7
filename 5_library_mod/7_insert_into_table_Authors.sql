use Kh_Mykich_Libary
go

-- insert data into table Authors
-- use sequense lib.seq_1 for column "Author_id"


insert into lib.Authors
(Author_id, Name, URL,  updated, updated_by) values 

-- row 26
(NEXT VALUE FOR lib.seq_1,'Mitchell_O.', 'www.mitchell.com', NULL, NULL),
-- row 27
(NEXT VALUE FOR lib.seq_1,'Kravchenko_A.', 'www.kravchenko.com', NULL, NULL),
-- row 28
(NEXT VALUE FOR lib.seq_1,'Ilon_mask', 'www.mask.en.com', NULL, NULL),
-- row 29
(NEXT VALUE FOR lib.seq_1,'Eshli_Vens', 'www.e_vens.en.com', NULL, NULL),
-- row 30
(NEXT VALUE FOR lib.seq_1,'Babych_Galyna', 'www.g_babych.com', NULL, NULL),
-- row 31
(NEXT VALUE FOR lib.seq_1,'Cheren_Liliya', 'www.l_cheryn.com', NULL, NULL),
-- row 32
(NEXT VALUE FOR lib.seq_1,'Klymenko_Lyuba', 'www.klym_l.ua.com', NULL, NULL),
-- row 33
(NEXT VALUE FOR lib.seq_1,'Hrymych_Maryna', 'www.mari.hr.ua.com', NULL, NULL),
-- row 34
(NEXT VALUE FOR lib.seq_1,'Baturyn_S.', 'www.b_serg.com.ua', NULL, NULL),
-- row 35
(NEXT VALUE FOR lib.seq_1,'Koval_Irena', 'www.Irena_K.com', NULL, NULL),
-- row 36
(NEXT VALUE FOR lib.seq_1,'Ryshkovets_Y.', 'www.ryshkovets_u.com', NULL, NULL),
-- row 37
(NEXT VALUE FOR lib.seq_1,'Vysotska_V.', 'www.victana.lviv.ua', NULL, NULL),
-- row 38
(NEXT VALUE FOR lib.seq_1,'Chyrun_L.', 'www.mask.en.com', NULL, NULL),
-- row 39
(NEXT VALUE FOR lib.seq_1,'Yakovyna_V.', 'www.y_vitaliy.com', NULL, NULL),
-- row 40
(NEXT VALUE FOR lib.seq_1,'Gavrysh_V.', 'www.v_gavrysh.com', NULL, NULL),
-- row 41
(NEXT VALUE FOR lib.seq_1,'Rishnyak_I.', 'www.igor_R.com', NULL, NULL),
-- row 42
(NEXT VALUE FOR lib.seq_1,'Shcherbak_S.', 'www.serhii.com.ua', NULL, NULL),
-- row 43
(NEXT VALUE FOR lib.seq_1,'Shyldt', 'www.g_sh.en.com/123', NULL, NULL),
-- row 44
(NEXT VALUE FOR lib.seq_1,'Pavlovska_T.', 'www.psvlov.T.com.ua', NULL, NULL),
-- row 45
(NEXT VALUE FOR lib.seq_1,'King_S.', 'www.king_st.com', NULL, NULL)

go
