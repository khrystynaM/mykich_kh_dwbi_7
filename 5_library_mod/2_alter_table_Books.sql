use Kh_Mykich_Libary
go

alter table lib.Books
add 
title varchar(20) not null default('Title'),
edition int not null default 1, check(edition >=1),
published date null,
issue int null
go

-- now, we must create composite primery key. Firstly, we must drop our primery key
-- but we must drop fireign key in table BooksAuthors

-- drop foreign key
alter table lib.BooksAuthors
drop constraint FK_ISBN 
go

-- drop primery key
alter table lib.Books
drop constraint PK_Books 
go


-- update date in table Books
update [lib].Books
set 
	edition = 200,
	published = '1998-10-12',
	issue = 1
	where ISBN = '900-6-323-014-1'
go

update [lib].Books
set 
	edition = 300,
	published = '2012-01-05',
	issue = 5
	where ISBN = '908-9-386-014-2'
go

update [lib].Books
set 
	edition = 100,
	published = '2017-11-17',
	issue = 6
	where ISBN = '909-8-329-014-1'
go
update [lib].Books
set 
	edition = 50,
	published = '2000-10-01',
	issue = 4
	where ISBN = '910-9-281-014-2'
go
update [lib].Books
set 
	edition = 5,
	published = '2018-02-11',
	issue = 6
	where ISBN = '918-2-346-634-2'
go
update [lib].Books
set 
	edition = 29,
	published = '2016-12-12',
	issue = 5
	where ISBN = '922-7-193-318-7'
go

update [lib].Books
set 
	edition = 1200,
	published = '1999-01-09',
	issue = 2
	where ISBN = '930-5-396-014-2'
go

update [lib].Books
set 
	edition = 120,
	published = '2001-02-08',
	issue = 1
	where ISBN = '934-3-267-694-5'
go

update [lib].Books
set 
	edition = 1010,
	published = '2009-05-30',
	issue = 3
	where ISBN = '934-6-173-314-1'
go

update [lib].Books
set 
	edition = 500,
	published = '2016-04-21',
	issue = 2
	where ISBN = '955-8-261-516-2'
go

update [lib].Books
set 
	edition = 250,
	published = '2014-09-08',
	issue = 5
	where ISBN = '966-5-386-014-2'
go

update [lib].Books
set 
	edition = 150,
	published = '2018-01-10',
	issue = 4
	where ISBN = '973-3-247-464-8'
go

update [lib].Books
set 
	edition = 2000,
	published = '2001-11-30',
	issue = 1
	where ISBN = '978-1-113-314-1'
go

update [lib].Books
set 
	edition = 2000,
	published = '2015-10-25',
	issue = 3
	where ISBN = '978-1-198-214-1'
go

update [lib].Books
set 
	edition = 350,
	published = '2017-07-26',
	issue = 5
	where ISBN = '978-1-211-014-2'
go

update [lib].Books
set 
	edition = 750,
	published = '2014-06-18',
	issue = 7
	where ISBN = '978-5-323-014-1'
go

update [lib].Books
set 
	edition = 2100,
	published = '1995-09-20',
	issue = 1
	where ISBN = '978-5-323-897-1'
go

update [lib].Books
set 
	edition = 1000,
	published = '2007-10-01',
	issue = 3
	where ISBN = '978-5-386-014-2'
go

update [lib].Books
set 
	edition = 700,
	published = '2017-04-03',
	issue = 4
	where ISBN = '979-4-291-014-2'
go

update [lib].Books
set 
	edition = 10000,
	published = '1999-08-30',
	issue = 1
	where ISBN = '988-1-113-094-7'
go

-- change NULL value for issue, because it primery key
alter table lib.Books
alter column issue int not null
go

-- now, we create composite PK
alter table lib.Books
add constraint PK_Books primary key (ISBN, issue) 
go


