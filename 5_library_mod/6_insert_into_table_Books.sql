use Kh_Mykich_Libary
go

-- insert data into table Books


insert into lib.Books
(ISBN, Publisher_Id, URL, Price, updated, updated_by, title, edition, published, issue) values 

-- row 26
('978-90-39-31-5', 34, 'www.Junior-English-Grammar', '240', NULL, NULL, 'Junior_English', 1000, '2017-09-09', 7 ),
-- row 27
('966-81-82-91-5', 29, 'www.kniga.org.ua/12523/', '412', NULL, NULL, 'Vidrodzhennya', 500, '2011-01-12', 1 ),
-- row 28
('271-90-11-29-6', 30, 'www.taobooks.org', '390', NULL, NULL, 'Tesla, SpaceX', 1100, '2018-02-06', 1 ),
-- row 29
('390-21-70-34-4', 27, 'www.duliby.com.ua/avtori/1', '95', NULL, NULL, 'Profesor_shumeiko', 2100, '2016-02-01',1 ),
-- row 30
('142-09-29-81-9', 27, 'www.duliby.com.ua/avtori/2', '100', NULL, NULL, 'Selo_Tarasovychi', 500, '2016-11-09', 1 ),
-- row 31
('121-06-25-09-9', 27, 'www.duliby.com.ua/36-marta', '75', NULL, NULL, 'Marta', 500, '2017-09-09', 1 ),
-- row 32
('431-11-01-33-4', 27, 'www.duliby.com.ua/34-vybr', '60', NULL, NULL, 'Vybrani_khity', 1500, '2014-11-11', 1 ),
-- row 33
('898-66-23-01-0', 31, 'www.kniga.org.ua/marg', '55', NULL, NULL, 'Ty_chyesh_Margo', 1000, '2012-07-26', 2 ),
-- row 34
('222-00-92-67-7', 32, 'www.kniga.org/magdal', '60', NULL, NULL, 'Magdalynky', 1500, '2016-03-11', 2 ),
-- row 35
('399-11-06-98-1', 26, 'www.duliby.com.ua/avtori/2', '100', NULL, NULL, 'Frida', 1500, '2012-05-29', 1 ),
-- row 36
('198-44-01-17-3', 35, 'www.kniga.org/baler', '70', NULL, NULL, 'Baleryna', 1000, '2011-12-11', 1 ),
-- row 37
('091-13-19-24-15', 29, 'www.kniga.org.ua/12523/', '190', NULL, NULL, 'Programyvannya_C', 40, '2017-10-30', 2 ),
-- row 38
('271-90-11-29-6', 35, 'www.kniga.org/C#', '190', NULL, NULL, 'C#', 3000, '2016-01-16', 3 ),
-- row 39
('314-99-91-22-7', 25, 'www.kniga.org/987623', '110', NULL, NULL, 'Metody_aproksymatsii', 105, '2006-06-18',1 ),
-- row 40
('093-11-17-77-8', 26, 'www.kniga.org/09111', '175', NULL, NULL, 'Kristina', 1800, '2017-08-23', 1 ),
-- row 41
('871-16-55-12-8', 25, 'www.book.ua/ukr/catalog', '110', NULL, NULL, 'Kerri', 500, '2018-02-19', 1 ),
-- row 42
('231-10-01-09-8', 33, 'www.diamail.com.ua/book/8290.', '230', NULL, NULL, 'Java: 10 v.', 2500, '2018-02-16', 3 ),
-- row 43
('777-17-23-01-0', 31, 'www.kniga.org.ua/PZ', '210', NULL, NULL, 'Testyvannya_PZ', 1000, '2012-07-26', 1 ),
-- row 44
('222-00-92-67-7', 32, 'www.kniga.org/CH_M', '150', NULL, NULL, 'Chyselni_metody', 100, '2016-03-11', 1 ),
-- row 45
('202-13-15-98-1', 26, 'www.duliby.com.ua/avtori/7', '100', NULL, NULL, 'Divchata', 500, '2016-01-29', 1 ),
-- row 46
('801-16-55-12-8', 25, 'www.book.ua/ukr/c786756', '130', NULL, NULL, 'Korabel', 600, '2015-10-09', 1 ),
-- row 47
('201-10-01-09-8', 33, 'www.book/8290.', '230', NULL, NULL, 'Web_programming', 1100, '2018-01-11', 3 ),
-- row 48
('707-17-23-01-0', 31, 'www.kniga.org.ua/maty_v', '210', NULL, NULL, 'Maty_vse', 3000, '2012-07-26', 1 ),
-- row 49
('202-00-94-67-9', 32, 'www.kniga.org/1+1', '110', NULL, NULL, 'Odyn_plus_odyn', 870, '2016-03-11', 1 ),
-- row 50
('299-13-17-98-3', 26, 'www.book/avtori/27', '190', NULL, NULL, 'Darunky', 500, '2016-01-29', 1 )


go
