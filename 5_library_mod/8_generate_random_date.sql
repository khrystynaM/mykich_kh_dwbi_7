use Kh_Mykich_Libary
go

-- generate random date for column bithday table Authors

DECLARE @StartDate date = '1800-01-01',
		@EndDate date = '2000-01-01',
		@birthday date
DECLARE @counter int, @i int

set @counter = (select count(*) from lib.Authors)
set @i = 1

while @i <= @counter
begin
	set @birthday = (select dateadd(day, rand(checksum(newid()))*(1+datediff(day, @StartDate, @EndDate)), @StartDate))
	
-- update table Authors

update lib.Authors
set
birthday = @birthday
from lib.Authors
where Author_Id = @i 
set @i = @i + 1

end
go
