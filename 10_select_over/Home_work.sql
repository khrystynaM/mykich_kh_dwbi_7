-- 1
-- �� ���������. ������������� �� ����� � ������� Trip � ������� ��������� �� ������. 
-- �������� ���������� �� {id_comp, trip_no}

select row_number() over (order by id_comp, trip_no) as num, trip_no, id_comp
from trip
go

-- 2
-- �� ���������. ������������� ����� ����� �������� ������
-- � ������� ��������� ������ �����.

select row_number() over (	partition by id_comp
							order by id_comp, trip_no) as num, trip_no, id_comp
from trip
go

-- 3
-- �� �����. �����. ������� ���������� ����� �������� �
-- ������ �������.


select model, color, type, price
from (
select *, rank() over(	partition by type 
						order by price) rank_printer
from printer
) pr
where rank_printer = 1
go

-- 4
-- �� �����. �����. ������ ���������, �� ����������
-- ����� 2-� ������� PC.

select maker 
from (
select maker, rank() over(	partition by maker
							order by model) rank_pc
from product 
where type ='pc'
) pc_r
where rank_pc > 2
go

-- 5
-- �� �����. �����. ������ ����� �� ��������� �������� ���� � ������� PC.

select price
from
(select price, dense_rank() over(order by price desc) as dense_rank_
from pc) pr
where dense_rank_ = 2
go

-- 6
-- �� ���������. ���������� �������� �� 3-� ������ ������.
-- ����� ������������ � ������� ��������� ������� (����� ����� � ������� Name).

select name, ntile(3) over (order by LastName) group_
from 
		(select  name,  
		left(name,charindex(' ',name)-1) as FirstName,  
		substring(name,charindex(' ',name)+1,len(name)) as LastName  
		from passenger) p
order by LastName asc
go

-- 7
-- �� �����. �����. ��� ������� PC ������� ����������, ��������� ��� ��������� ��������
-- (�������� �� �������). ������� ���� ������� ���� ����������� �� ������� price �� ���������.
-- ����� ������� ������� ������ �� 3 ������. ���� �� ���� �� �� ������� �����

select code, model, speed, ram, hd, cd, price,
row_number() over (order by price desc) as id,
count(*) over()   AS row_total,
ntile(4) over(order by code) as page_num,
(select ceiling(count(*)/3.0) from pc) as page_total
from pc
go

-- 8
-- �� �Գ��� ����. ������������. ������ ����������� ���� �������/������ ����� ��� 4-� ������� ���� �����,
-- � ����� ��� ��������, ���� � ����� �������, ���� � �� ���� ���� �����������.

-- create cte 1
; with cte_1 as
(select inc as sum, 'inc' as type, date, point
from income

union all
select inc as sum, 'inc' as type, date, point
from income_o

union all

select out as sum, 'out' as type, date, point
from outcome

union all

select out as sum, 'out' as type, date, point
from outcome_o), 

-- create cte 2
cte_2 as
(select sum, type, date, point,
  		max(sum) over(partition by sum ) as sup_quantity,
		rank() over(order by sum desc) as sum_rank
		from  cte_1)

-- select data
		select sum as max_sum, type, date, point
		from cte_2  
		where sum_rank = 1
		go

-- 9
-- �� �����. �����. ��� ������� �� � ������� PC ������ ������ �� ���� ����� � ��������� ����� �� ����� �
-- ����� �� ��������� �������� ��, � ����� �� ���� ����� �� ��������� ��������� ����� �� �������. 

select *, avg(price) over() as total_price
from (
select *, price - avg(price) over() as dif_total_price 
from (
select *, price - avg(price) over(partition by speed) as dif_local_price
from pc) sq1) sq2
go