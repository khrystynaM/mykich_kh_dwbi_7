﻿-- 1
--  БД «Комп. фірма». Знайти виробників ноутбуків. Вивести: maker, type.
--  Вихідні дані впорядкувати за зростанням за стовпцем maker.

use labor_sql
go

select distinct maker, type
from product
where type = 'laptop'
order by maker asc
go

-- 2
-- БД «Комп. фірма». Знайти номер моделі, об’єм пам’яті та розміри екранів ноутбуків, 
-- ціна яких перевищує 1000 дол. Вивести: model, ram, screen, price. Вихідні дані впорядкувати
-- за зростанням за стовпцем ram та за спаданням за стовпцем price.

use labor_sql
go

select model, ram, screen, price
from laptop
where price > 1000
order by ram asc, price desc
go

-- 3
-- БД «Комп. фірма». Знайдіть усі записи таблиці Printer для кольорових принтерів.
-- Вихідні дані впорядкувати за спаданням за стовпцем price.

use labor_sql
go

select code, model, type, price
from printer
where color = 'y'
order by price desc
go

-- 4
-- БД «Комп. фірма». Знайдіть номер моделі, швидкість та розмір диску ПК, що мають CD-приводи
-- зі швидкістю '12х' чи '24х' та ціну меншу 600 дол. Вивести: model, speed, hd, cd, price.
-- Вихідні дані впорядкувати за спаданням за стовпцем speed.

use labor_sql
go

select model, speed, hd, cd, price
from pc
where (cd = '12x' or cd = '24x') and price < 600
order by speed desc
go

-- 5
-- БД «Кораблі». Перерахувати назви головних кораблів (з таблиці Ships). 
-- Вивести: name, class. Вихідні дані впорядкувати за зростанням за стовпцем name.

use DB_ships
go

select name, class
from ships
where name like class
order by name asc
go

-- 6
-- БД «Комп. фірма». Отримати інформацію про комп’ютери, що мають частоту процесора не менше 500 МГц 
-- та ціну нижче 800 дол. 
-- Вихідні дані впорядкувати за спаданням за стовпцем price.

use labor_sql
go

select model, speed, ram, hd, cd, price
from pc
where speed >= 500 and price < 800
order by price desc
go

-- 7
-- БД «Комп. фірма». Отримати інформацію про всі принтери, які не є матричними
-- та коштують менше 300 дол. Вихідні дані впорядкувати за спаданням за стовпцем type.

use labor_sql
go

select model, color, type, price
from printer
where type != 'Matrix' and price < 300
order by type desc
go

-- 8
-- БД «Комп. фірма». Знайти модель та частоту процесора комп’ютерів, що коштують
--  від 400 до 600 дол. Вивести: model, speed. Вихідні дані впорядкувати 
-- за зростанням за стовпцем hd

use labor_sql
go

select model, speed
from pc
where price between 400 and 600
order by hd asc
go

-- 9
--  БД «Комп. фірма». Знайдіть номер моделі, швидкість та розмір жорсткого диску для усіх ноутбуків, 
-- екран яких не менше 12 дюймів. Вивести: model, speed, hd, price. Вихідні дані
-- впорядкувати за спаданням за стовпцем price.

use labor_sql
go

select model, speed, hd, price
from laptop
where hd < 12
order by price desc
go

-- 10
-- БД «Комп. фірма». Знайдіть номер моделі, тип та ціну для усіх принтерів,
-- вартість яких менше 300 дол. Вивести: model, type, price. 
-- Вихідні дані впорядкувати за спаданням за стовпцем type.

use labor_sql
go

select model, type, price
from printer
where price < 300
order by type desc
go

-- 11
-- БД «Комп. фірма». Вивести моделі ноутбуків з кількістю RAM рівною 64 Мб. 
-- Вивести: model, ram, price. Вихідні дані впорядкувати за зростанням за стовпцем screen.

use labor_sql
go

select model, ram, price
from laptop
where ram = 64
order by screen asc
go

-- 12
-- БД «Комп. фірма». Вивести моделі ПК з кількістю RAM більшою за 64 Мб.
-- Вивести: model, ram, price. Вихідні дані впорядкувати за зростанням за стовпцем hd.

use labor_sql
go

select model, ram, price
from laptop
where ram > 64
order by hd asc
go

-- 13
-- БД «Комп. фірма». Вивести моделі ПК зі швидкістю процесора у межах від 500 до 750 МГц. 
-- Вивести: model, speed, price. Вихідні дані впорядкувати за спаданням за стовпцем hd.

use labor_sql
go

select model, speed, price
from laptop
where speed between 500 and 750
order by hd desc
go

-- 14
-- БД «Фірма прий. вторсировини». Вивести інформацію про видачу грошей на суму понад 2000 грн. 
-- на пунктах прийому таблиці Outcome_o. 
-- Вихідні дані впорядкувати за спаданням за стовпцем date.

use DB_Inc_Out
go

select point, date, out
from outcome_o
where out > 2000
order by date desc
go

-- 15
--  БД «Фірма прий. вторсировини». Вивести інформацію про прийом грошей на суму
--  у межах від 5 тис. до 10 тис. грн. на пунктах прийому таблиці Income_o.
--  Вихідні дані впорядкувати за зростанням за стовпцем inc.

use DB_Inc_Out
go

select point, date, inc
from income_o
where inc between  5000 and 10000
order by inc asc
go

-- 16
-- БД «Фірма прий. вторсировини». Вивести інформацію про прийом грошей на пункті прийому №1 таблиці Income.
-- Вихідні дані впорядкувати за зростанням за стовпцем inc.

use DB_Inc_Out
go

select date, inc
from income
where point = 1
order by inc asc
go

-- 17
-- БД «Фірма прий. вторсировини». Вивести інформацію про видачу грошей на пункті прийому №2 таблиці Outcome.
-- Вихідні дані впорядкувати за зростанням за стовпцем out.

use DB_Inc_Out
go

select date, out
from outcome
where point = 2
order by out asc
go

-- 18
-- БД «Кораблі». Вивести інформацію про усі класи кораблів для країни 'Japan'. 
-- Вихідні дані впорядкувати за спаданням за стовпцем type.

use DB_ships
go

select class, type, numGuns, bore, displacement
from classes
where country = 'Japan'
order by type desc
go

-- 19
-- БД «Кораблі». Знайти всі кораблі, що були спущені на воду у
-- термін між 1920 та 1942 роками. Вивести: name, launched.
-- Вихідні дані впорядкувати за спаданням за стовпцем launched.

use DB_ships
go

select name, launched
from ships
where launched between 1920 and 1942
order by launched desc
go

-- 20
-- БД «Кораблі». Вивести усі кораблі, що брали участь у битві 'Guadalcanal'
-- та не були потопленими. Вивести: ship, battle, result.
-- Вихідні дані впорядкувати за спаданням за стовпцем ship.

use DB_ships
go

select ship, battle, result
from outcomes
where result != 'sunk'
order by ship desc
go

-- 21
--  БД «Кораблі». Вивести усі потоплені кораблі. Вивести: ship, battle, result.
--  Вихідні дані впорядкувати за спаданням за стовпцем ship.

use DB_ships
go

select ship, battle, result
from outcomes
where result = 'sunk'
order by ship desc
go

-- 22
-- БД «Кораблі». Вивести назви класів кораблів з водотоннажністю не меншою, аніж 40 тон. 
-- Вивести: class, displacement. 
-- Вихідні дані впорядкувати за зростанням за стовпцем type.

use DB_ships
go

select class, displacement
from classes
where displacement >= 40000
order by type asc
go

-- 23
-- БД «Аеропорт». Знайдіть номера усіх рейсів, що бувають у місті 'London'. 
-- Вивести: trip_no, town_from, town_to. Вихідні дані впорядкувати за зростанням за стовпцем time_out.

use DB_airport
go

select trip_no, town_from, town_to
from trip
where town_from = 'London' or town_to = 'London'
order by time_out asc
go


-- 24
-- БД «Аеропорт». Знайдіть номера усіх рейсів, на яких курсує літак 'TU-134'. 
-- Вивести: trip_no, plane, town_from, town_to.
-- Вихідні дані впорядкувати за спаданням за стовпцем time_out.

use DB_airport
go

select trip_no, plane, town_from, town_to
from trip
where plane = 'TU-134'
order by time_out desc
go

-- 25
-- БД «Аеропорт». Знайдіть номера усіх рейсів, на яких не курсує літак 'IL-86'.
-- Вивести: trip_no, plane, town_from, town_to. Вихідні дані впорядкувати за зростанням за стовпцем plane.

use DB_airport
go

select trip_no, plane, town_from, town_to
from trip
where plane not in ('IL-86')
order by plane asc
go

-- 26
-- БД «Аеропорт». Знайдіть номера усіх рейсів, що не бувають у місті 'Rostov'. 
-- Вивести: trip_no, town_from, town_to. Вихідні дані впорядкувати за зростанням за стовпцем plane.

use DB_airport
go

select trip_no, town_from, town_to
from trip
where town_from != 'Rostov' and town_to != 'Rostov'
order by plane asc
go

-- 27
-- БД «Комп. фірма». Вивести усі моделі ПК, у номерах яких є хоча б дві одинички.

use labor_sql
go

select model
from pc
where model like '%1%1%'
go

-- 28
-- БД «Фірма прий. вторсировини». З таблиці Outcome вивести усю інформацію за березень місяць.

use DB_Inc_Out
go

select code, point, date, out
from outcome
where datepart ( mm, date ) = 3
go

-- 29
-- БД «Фірма прий. вторсировини». З таблиці Outcome_o вивести усю інформацію за 14 число будь-якого місяця.

use DB_Inc_Out
go

select point, date, out
from outcome_o
where datepart ( day, date ) = 14
go

-- 30 
-- БД «Кораблі». З таблиці Ships вивести назви кораблів, що починаються на 'W' та закінчуються літерою 'n'.

use DB_ships
go

select name
from ships
where name like 'W%n'
go

-- 31
-- БД «Кораблі». З таблиці Ships вивести назви кораблів, що мають у своїй назві дві літери 'e'.

use DB_ships
go

select name
from ships
where name like '%e%e%'
go

-- 32
-- БД «Кораблі». З таблиці Ships вивести назви кораблів та роки їх спуску на воду,
-- назва яких не закінчується на літеру 'a'.

use DB_ships
go

select name, launched
from ships
where name not like '%a'
go

-- 33
-- БД «Кораблі». Вивести назви битв, які складаються з двох слів
-- та друге слово не закінчується на літеру 'c'.

use DB_ships
go

select name
from battles
where name  like '% %c'
go

-- 34
-- БД «Аеропорт». З таблиці Trip вивести інформацію про рейси,
-- що вилітають в інтервалі часу між 12 та 17 годинами включно

use DB_airport
go

select trip_no, plane, town_from, town_to, time_out
from trip
where datepart (hour, time_out ) between 12 and 17
go

-- 35
-- БД «Аеропорт». З таблиці Trip вивести інформацію про рейси, що прилітають в інтервалі часу 
-- між 17 та 23 годинами включно.

use DB_airport
go

select trip_no, plane, town_from, town_to, time_in
from trip
where datepart (hour, time_in ) between 17 and 23
go

-- 36
-- БД «Аеропорт». З таблиці Trip вивести інформацію про рейси, що прилітають в інтервалі часу 
-- між 21 та 10 годинами включно.

use DB_airport
go

select trip_no, plane, town_from, town_to, time_in
from trip
where datepart (hour, time_in ) between 21 and 23
or datepart (hour, time_in ) between 00 and 10
go

-- 37
-- БД «Аеропорт». З таблиці Pass_in_trip вивести дати, коли були зайняті місця у першому ряду.

use DB_airport
go

select date
from pass_in_trip
where place like '1%'
go

-- 38
-- БД «Аеропорт». З таблиці Pass_in_trip вивести дати, коли були зайняті місця 'c' у будь-якому ряді

use DB_airport
go

select date
from pass_in_trip
where place like '%c'
go

-- 39
-- БД «Аеропорт». Вивести прізвища пасажирів (друге слово у стовпці name), що починаються на літеру 'С'.

use DB_airport
go

select name
from passenger 
where name like '% C%'
go

-- 40
-- БД «Аеропорт». Вивести прізвища пасажирів (друге слово у стовпці name), що не починаються на літеру 'J'.

use DB_airport
go

select name
from passenger 
where name not like '% J%'
go

-- 41
-- БД «Комп. фірма». Виведіть середню ціну ноутбуків з попереднім текстом 'середня ціна = '.

use labor_sql
go

select avg(price) as 'середня ціна ='
from laptop
go

-- 42
-- БД «Комп. фірма». Для таблиці PC вивести усю інформацію з коментарями у кожній комірці, 
-- наприклад, 'модель: 1121', 'ціна: 600,00' і т.д.

use labor_sql
go

select  concat (N'модель:', model) as model,
		concat (N'частота:', speed) as speed,
		concat (N'об’єм пам’яті:', ram) as ram,
		concat (N'розмір диску:', hd) as hd,
		concat (N'швидкість CD-приводу:', cd) as сd,
		concat (N'ціна:', price) as price 
from pc
go

-- 43
-- БД «Фірма прий. вторсировини». З таблиці Income виведіть дати у такому форматі: 
-- рік.число_місяця.день, наприклад, 2001.02.15 (без формату часу).

use DB_Inc_Out
go

select replace(convert(date, i.date), '-', '.') as new_date_format
from income i
go

-- 44
-- БД «Кораблі». Для таблиці Outcomes виведіть дані, а заміть значень стовпця result,
-- виведіть еквівалентні їм надписи українською мовою.

use DB_ships
go

select ship, battle,
(case result 
when 'sunk' then N'затоплений'
when 'damaged' then N'пошкоджений'
else N'із кораблем все добре'
end) as стан
from outcomes
go

-- 45
-- БД «Аеропорт». Для таблиці Pass_in_trip значення стовпця place розбити на два стовпця 
-- з коментарями, наприклад, перший – 'ряд: 2' та другий – 'місце: a'.

use DB_airport
go

 select trip_no,
		date,
		id_psg,
		substring(place,1,1)  as number,
		substring(place,2,1)  as place 
 from pass_in_trip 
 go
 
 -- 46
 -- БД «Аеропорт». Вивести дані для таблиці Trip з об’єднаними значеннями двох стовпців:
 -- town_from та town_to, з додатковими коментарями типу: 'from Rostov to Paris'.

use DB_airport
go

select trip_no, id_comp, plane, time_out, time_in,
concat('from ', trim(town_from), 'to ', trim(town_to)) as way
from trip
go

 -- 48
 -- БД «Комп. фірма». Знайдіть виробників, що випускають по крайній мірі дві різні моделі ПК.
 -- Вивести: maker, число моделей.

use labor_sql
go

select maker, count( distinct model) as count_model
from product
where type = 'pc'
group by maker
having count( distinct model) >= 2
go

-- 49
-- БД «Аеропорт». Для кожного міста порахувати кількість рейсів, що у ньому курсують (прилітають, відлітають)

use DB_airport
go

select distinct town_from, count(town_from) + count(town_to) as count_all
from trip  
group by town_from
go


-- 50
-- БД «Комп. фірма». Порахуйте для кожного типу принтера
-- кількість наявних моделей.

use labor_sql
go

select type, count(model) as model_count
from printer
group by type
go

-- 51
-- БД «Комп. фірма». Для моделей в таблиці PC порахуйте, скільки є різних cd 
-- (тобто з різною швидкістю), а також для кожної швидкості вкажіть наявну кількість моделей

use labor_sql
go

select distinct model, count(distinct cd) as different_cd
from pc
group by model
go

--
use labor_sql
go
select cd, count(model) as model_count
from pc
group by cd 
go

-- 52
-- БД «Аеропорт». Для кожного рейсу (таблиця Trip) визначити тривалість його польоту

use DB_airport
go

select  trip_no, convert(time, time_in - time_out) as flight_time 
from trip t
go

-- 53
-- БД «Фірма прий. вторсировини». Для таблиці Outcome для кожного пункту порахувати суму грошей, 
-- як за кожне число, так і за всі дати, а також вивести для них мінімальну та максимальну суми.

use DB_Inc_Out
go
  select point, date, sum(out) as sum_of_point, min(out) as min_of_point, max(out) as max_of_point
  from outcome
  group by point, date with rollup
  go

-- 54
-- БД «Аеропорт». Для таблиці Pass_in_trip для кожного рейсу (trip_no) порахувати, 
-- скільки було зайнято місць у кожному ряді.

use DB_airport
go

select distinct trip_no,
		substring(place,1,1)  as row_number,
		count(substring(place,2,1))  as count_busy_place_in_row 
from pass_in_trip 
group by  substring(place,1,1), trip_no
go

-- 55
-- БД «Аеропорт». Для таблиці Passenger порахувати скільки було пасажирів, 
-- прізвища яких починаються на літеру S, B та А.

use DB_airport
go

select count(name) as count_passenger
from passenger
where name like '% [SBA]%'
go
