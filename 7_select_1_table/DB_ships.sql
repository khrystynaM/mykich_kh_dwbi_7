use master 
go

drop database if exists DB_ships
go

create database DB_ships
go

use DB_ships
go

CREATE TABLE [battles] (
	[name] varchar (20) NOT NULL ,
	[date] datetime NOT NULL 
); 

CREATE TABLE [classes] (
	[class]   varchar (50) NOT NULL ,
	[type]    varchar (2) NOT NULL ,
	[country] varchar (20) NOT NULL ,
	[numGuns] tinyint NULL ,
	[bore]    real NULL ,
	[displacement] int NULL 
); 

CREATE TABLE [ships] (
	[name]     varchar (50) NOT NULL ,
	[class]    varchar (50) NOT NULL ,
	[launched] smallint NULL 
); 

CREATE TABLE [outcomes] (
	[ship]   varchar (50) NOT NULL ,
	[battle] varchar (20) NOT NULL ,
	[result] varchar (10) NOT NULL 
); 

ALTER TABLE [battles] ADD 
	CONSTRAINT PK_battles PRIMARY KEY  CLUSTERED 
	([name]);   

ALTER TABLE [classes] ADD 
	CONSTRAINT PK_classes PRIMARY KEY  CLUSTERED 
	([class]);   

ALTER TABLE [ships] ADD 
	CONSTRAINT PK_ships PRIMARY KEY  CLUSTERED 
	([name]);   

ALTER TABLE [outcomes] ADD 
	CONSTRAINT PK_outcomes PRIMARY KEY  CLUSTERED 
	([ship], [battle]);   

ALTER TABLE [ships] ADD 
	CONSTRAINT FK_ships_Classes FOREIGN KEY ([class]) 
	REFERENCES [classes] ([class]);

ALTER TABLE [outcomes] ADD 
	CONSTRAINT FK_outcomes_battles FOREIGN KEY ([battle]) 
	REFERENCES [battles] ([name]);
                                                                                                                                                                                                                                                               
---[classes]--------------------------------------------------------
insert into [classes] values('Bismarck','bb','Germany',8,15,42000);
insert into [classes] values('Iowa','bb','USA',9,16,46000);
insert into [classes] values('Kon','bc','Japan',8,14,32000);
insert into [classes] values('North Carolina','bb','USA',12,16,37000);
insert into [classes] values('Renown','bc','Gt.Britain',6,15,32000);
insert into [classes] values('Revenge','bb','Gt.Britain',8,15,29000);
insert into [classes] values('Tennessee','bb','USA',12,14,32000);
insert into [classes] values('Yamato','bb','Japan',9,18,65000);
                                                                                                                                                                                                                                                                 
---[battles]----------------------------------------------------------
insert into [battles] values('Guadalcanal','1942-11-15 00:00:00.000');
insert into [battles] values('North Atlantic','1941-05-25 00:00:00.000');
insert into [battles] values('North Cape','1943-12-26 00:00:00.000');
insert into [battles] values('Surigao Strait','1944-10-25 00:00:00.000');
insert into [battles] values ('#Cuba62a'   , '1962-10-20');
insert into [battles] values ('#Cuba62b'   , '1962-10-25');
                                                                                                                                                                                                                                                                 
---[ships]-------------------------------------------------------------
insert into [ships] values('California','Tennessee',1921);
insert into [ships] values('Haruna','Kon',1916);
insert into [ships] values('Hiei','Kon',1914);
insert into [ships] values('Iowa','Iowa',1943);
insert into [ships] values('Kirishima','Kon',1915);
insert into [ships] values('Kon','Kon',1913);
insert into [ships] values('Missouri','Iowa',1944);
insert into [ships] values('Musashi','Yamato',1942);
insert into [ships] values('New Jersey','Iowa',1943);
insert into [ships] values('North Carolina','North Carolina',1941);
insert into [ships] values('Ramillies','Revenge',1917);
insert into [ships] values('Renown','Renown',1916);
insert into [ships] values('Repulse','Renown',1916);
insert into [ships] values('Resolution','Renown',1916);
insert into [ships] values('Revenge','Revenge',1916);
insert into [ships] values('Royal Oak','Revenge',1916);
insert into [ships] values('Royal Sovereign','Revenge',1916);
insert into [ships] values('Tennessee','Tennessee',1920);
insert into [ships] values('Washington','North Carolina',1941);
insert into [ships] values('Wisconsin','Iowa',1944);
insert into [ships] values('Yamato','Yamato',1941);
insert into [ships] values('South Dakota','North Carolina',1941); 
                                                                                                                                                                                                                                                             
---[outcomes]------------------------------------------------------
insert into [outcomes] values('Bismarck','North Atlantic','sunk');
insert into [outcomes] values('California','Surigao Strait','OK');
insert into [outcomes] values('Duke of York','North Cape','OK');
insert into [outcomes] values('Fuso','Surigao Strait','sunk');
insert into [outcomes] values('Hood','North Atlantic','sunk');
insert into [outcomes] values('King George V','North Atlantic','OK');
insert into [outcomes] values('Kirishima','Guadalcanal','sunk');
insert into [outcomes] values('Prince of Wales','North Atlantic','damaged');
insert into [outcomes] values('Rodney','North Atlantic','OK');
insert into [outcomes] values('Schamhorst','North Cape','sunk');
insert into [outcomes] values('South Dakota','Guadalcanal','damaged');
insert into [outcomes] values('Tennessee','Surigao Strait','OK');
insert into [outcomes] values('Washington','Guadalcanal','OK');
insert into [outcomes] values('West Virginia','Surigao Strait','OK');
insert into [outcomes] values('Yamashiro','Surigao Strait','sunk');
insert into [outcomes] values('California','Guadalcanal','damaged');