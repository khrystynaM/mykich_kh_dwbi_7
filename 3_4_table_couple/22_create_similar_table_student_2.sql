use K_M_module_3
go

drop table if exists edu.student_2
go

--- create table 4 - student_2
CREATE TABLE edu.student_2 (
       student_id           integer NOT NULL,
       first_name			varchar(20)  NULL,
	   last_name			varchar(20)  NULL,
	   middle_name			varchar(20)  NULL,
	   certificate_number   varchar(20)  NULL,
	   instituation			varchar(50)  NULL,
	   department			varchar(45)  NULL,
	   speciality			varchar(45)  NULL,
	   age					integer      NULL,
	   year_of_introduction	date		 NULL,
	   phone_number         integer		 NULL,
	   type_operation		varchar(20) NULL,
	   date_operation		date NULL
	 
) on [Primary]
go