use K_M_module_3
go

drop trigger if exists edu.tr_stud_ins 
go

---- trigger creation
create trigger edu.tr_stud_ins  ON edu.student
AFTER  INSERT 
AS
BEGIN

insert into edu.student_2
		([student_id]
		,[first_name]
		,[last_name]
		,[middle_name]
		,[certificate_number]
		,[instituation]
		,[department]
		,[speciality]
		,[age]
		,[year_of_introduction]
		,[phone_number]
		,[type_operation]
		,[date_operation])
	Select 
		 inserted.[student_id]
		,inserted.[first_name]
		,inserted.[last_name]
		,inserted.[middle_name]
		,inserted.[certificate_number]
		,inserted.[instituation]
		,inserted.[department]
		,inserted.[speciality]
		,inserted.[age]
		,inserted.[year_of_introduction]
		,inserted.[phone_number]
		,'insert'
		,getdate()
	FROM edu.student
	inner join inserted on edu.student.[student_id] = inserted.[student_id]

END