use K_M_module_3
go

-- simple select statement table subject
select *
from edu.[subject]
go


----insert new record, where [amount_of_hour] = 0
----insert first record
insert into [edu].[subject]
           ([subject_id]
           ,[subject_name]
           ,[subject_code]
           ,[description_subject]
		   ,[teacher_name] 
		   ,[amount_of_hour]
		   ,[type_of_subject]
		   ,[course_of_studying]
		   ,[year_of_creation])
     values
           (11
           ,'Logika'
           ,'U8795'
           ,'Solve hard task'
		   ,'Veres_Ihor'
		   ,'0'
		   ,'Primary'
		   ,'3'
		   ,'2000')

go

-- simple select statement to check for incorrect data insertion
select *
from edu.[subject]
go