use K_M_module_3
go


drop trigger if exists edu.tr_subject
go

---- trigger creation
create trigger tr_subject ON edu.[subject]
AFTER UPDATE
AS
BEGIN
	update edu.[subject]
	set updated_date = getdate()
	from edu.subject
		inner join inserted on edu.[subject].subject_id = inserted.subject_id
END
GO