use K_M_module_3
go


-- update data
update edu.subject_student
set mark = 79
where date_exam = '2017-12-25'

-- column "updated_date" must be change 
-- check this
select subject_id, student_id, mark, date_exam, updated_date
from edu.subject_student
