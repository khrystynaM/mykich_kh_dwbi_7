use K_M_module_3
go

insert into [edu].[student]
           ([student_id]
		   ,[last_name]
           ,[first_name]
           ,[middle_name]
		   ,[certificate_number] 
		   ,[instituation]
		   ,[department]
		   ,[speciality]
		   ,[age]
		   ,[year_of_introduction]
		   ,[phone_number]
		   ,[inserted_date]
		   ,[updated_date])
     values
           (9
           ,'Vasylevych'
           ,'Ivan'
           ,'Dmytrovych'
		   ,'KN12005'
		   ,'Computer_science_and_information_technology'
		   ,'Information_systems_and_networks'
		   ,'Computer_science'
		   ,'20'
		   ,'2017-08-29'
		   ,'0972378678'
		   ,'2017'
		   ,'2017'),
		   
		   (10
           ,'Vitoshko'
           ,'Taras'
           ,'Dmytrovych'
		   ,'KN19905'
		   ,'Computer_science_and_information_technology'
		   ,'Information_systems_and_networks'
		   ,'Computer_science'
		   ,'20'
		   ,'2017-08-27'
		   ,'0972378238'
		   ,'2017'
		   ,'2017')
go

-- check our new table student_2
select *
from edu.student_2
go