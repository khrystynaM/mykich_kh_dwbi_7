use K_M_module_3
go

-- simple select statement table subject_student
select *
from edu.[subject_student]
go

----insert new record, where mark > 100
insert into [edu].[subject_student]
           ([subject_id]
		   ,[student_id]
           ,[mark]
           ,[date_exam])
     values
           (8
           ,'1'
           ,'101'
		   ,'2017-09-29')

go



-- simple select statement to check for incorrect data insertion
select *
from edu.[subject_student]
go