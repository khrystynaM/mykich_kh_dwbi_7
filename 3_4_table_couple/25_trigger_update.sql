use K_M_module_3
go

drop trigger if exists edu.tr_stud_upd
go

---- trigger creation
create trigger edu.tr_stud_upd   ON edu.student
AFTER  UPDATE
AS
BEGIN

insert into edu.student_2
		([student_id]
		,[last_name]
		,[first_name]
		,[middle_name]
		,[certificate_number]
		,[instituation]
		,[department]
		,[speciality]
		,[age]
		,[year_of_introduction]
		,[phone_number]
		,[type_operation]
		,[date_operation])
select 
		 i.[student_id]
		,i.[last_name]
		,i.[first_name]
		,i.[middle_name]
		,i.[certificate_number]
		,i.[instituation]
		,i.[department]
		,i.[speciality]
		,i.[age]
		,i.[year_of_introduction]
		,i.[phone_number]
		,'update'
		,getdate()
	FROM inserted i

END