use K_M_module_3
go

--- create view (table subject)
CREATE OR ALTER VIEW edu.subject_view
(
	   [subject_id]
      ,[subject_name]
      ,[subject_code]
      ,[description_subject]
      ,[teacher_name]
      ,[amount_of_hour]
      ,[type_of_subject]
      ,[course_of_studying]
	  ,[year_of_creation]
  )
  AS
  SELECT  [subject_id]
      ,[subject_name]
      ,[subject_code]
      ,[description_subject]
      ,[teacher_name]
      ,[amount_of_hour]
      ,[type_of_subject]
      ,[course_of_studying]
	  ,[year_of_creation]

  FROM [edu].[subject]

go
-- select data from our view
select *
from edu.subject_view