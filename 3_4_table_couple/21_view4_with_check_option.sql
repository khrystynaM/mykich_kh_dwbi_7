use K_M_module_3
go

  CREATE OR ALTER VIEW edu.student_view_check
  as
  SELECT
	   [student_id]
      ,[first_name]
      ,[last_name]
      ,[middle_name]
	  ,[phone_number]
	  ,[department]
FROM [edu].[student]
WHERE [department] = 'Information_systems_and_networks'
WITH CHECK OPTION
go

-- select data from our view
select *
from edu.student_view_check
