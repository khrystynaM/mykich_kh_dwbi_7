use [K_M_module_3]
go

----insert new record

----insert first record
insert into [edu].[subject]
           ([subject_id]
           ,[subject_name]
           ,[subject_code]
           ,[description_subject]
		   ,[teacher_name] 
		   ,[amount_of_hour]
		   ,[type_of_subject]
		   ,[course_of_studying]
		   ,[year_of_creation])
     values
           (1
           ,'Database'
           ,'U8765'
           ,'Creation database and their support'
		   ,'Veres_Oleg'
		   ,'235'
		   ,'Primary'
		   ,'3'
		   ,'2000')

go

----insert new record
----insert second record
insert into [edu].[subject]
           ([subject_id]
           ,[subject_name]
           ,[subject_code]
           ,[description_subject]
		   ,[teacher_name] 
		   ,[amount_of_hour]
		   ,[type_of_subject]
		   ,[course_of_studying]
		   ,[year_of_creation])
     values
           (2
           ,'Programming'
           ,'U8766'
           ,'Creation program using Java'
		   ,'Sokil_Igor'
		   ,'250'
		   ,'Primary'
		   ,'2'
		   ,'2016')

go

----insert third record
insert into [edu].[subject]
           ([subject_id]
           ,[subject_name]
           ,[subject_code]
           ,[description_subject]
		   ,[teacher_name] 
		   ,[amount_of_hour]
		   ,[type_of_subject]
		   ,[course_of_studying]
		   ,[year_of_creation])
     values
           (3
           ,'Japanese_language'
           ,'U8723'
           ,'Studying the basics of the Japanese language. Includes lectures and practical classes'
		   ,'Behta_Mariya'
		   ,'170'
		   ,'Additional'
		   ,'1'
		   ,'2015')

go

----insert fourth record
insert into [edu].[subject]
           ([subject_id]
           ,[subject_name]
           ,[subject_code]
           ,[description_subject]
		   ,[teacher_name] 
		   ,[amount_of_hour]
		   ,[type_of_subject]
		   ,[course_of_studying]
		   ,[year_of_creation])
     values
           (4
           ,'Linear_algebra'
           ,'U8711'
           ,'Study the basics of linear algebra. Matrix'
		   ,'Bobelyuk_Taras'
		   ,'217'
		   ,'Primary'
		   ,'1'
		   ,'1998')

go

----insert fifth record
insert into [edu].[subject]
           ([subject_id]
           ,[subject_name]
           ,[subject_code]
           ,[description_subject]
		   ,[teacher_name] 
		   ,[amount_of_hour]
		   ,[type_of_subject]
		   ,[course_of_studying]
		   ,[year_of_creation])
     values
           (5
           ,'Software_simulation'
           ,'U2387'
           ,'A software simulation is a model of your software that allows you to demonstrate its key functions and operations. Using software simulations, you can show your customer how everything works in your program. '
		   ,'Spivak_Iryna'
		   ,'289'
		   ,'Primary'
		   ,'3'
		   ,'2015')

go
----insert sixth record
insert into [edu].[subject]
           ([subject_id]
           ,[subject_name]
           ,[subject_code]
           ,[description_subject]
		   ,[teacher_name] 
		   ,[amount_of_hour]
		   ,[type_of_subject]
		   ,[course_of_studying]
		   ,[year_of_creation])
     values
           (6
           ,'Common_hygiene'
           ,'U2399'
           ,'In this lesson, you will learn what hygiene factors are and the key concepts behind them'
		   ,'Vertova_Nataliya'
		   ,'240'
		   ,'Primary'
		   ,'2'
		   ,'2001')

go

----insert seventh record
insert into [edu].[subject]
           ([subject_id]
           ,[subject_name]
           ,[subject_code]
           ,[description_subject]
		   ,[teacher_name] 
		   ,[amount_of_hour]
		   ,[type_of_subject]
		   ,[course_of_studying]
		   ,[year_of_creation])
     values
           (7
           ,'Politology'
           ,'U5698'
           ,'Political scientists study matters concerning the allocation and transfer of power in decision making, the roles and systems of governance including governments and international organizations, political behaviour and public policies. They measure the success of governance and specific policies by examining many factors, including stability, justice, material wealth, peace and public health'
		   ,'Novikov_Ivan'
		   ,'310'
		   ,'Primary'
		   ,'1'
		   ,'2001')

----insert eighth record
insert into [edu].[subject]
           ([subject_id]
           ,[subject_name]
           ,[subject_code]
           ,[description_subject]
		   ,[teacher_name] 
		   ,[amount_of_hour]
		   ,[type_of_subject]
		   ,[course_of_studying]
		   ,[year_of_creation])
     values
           (8
           ,'Town_planning'
           ,'U5600'
           ,'Town Planning involves both control of existing and new development, and "strategic planning" to ensure our resources are carefully managed to match our future needs and expectations'
		   ,'Oleskivskyi_Petro'
		   ,'210'
		   ,'Primary'
		   ,'1'
		   ,'2006')


-- select all records from table subject
select * from [edu].[subject]
go
