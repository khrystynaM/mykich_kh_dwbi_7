use K_M_module_3
go


-- simple select statement table student
select student_id, last_name, age
from edu.student
go

-- Such student doesn't exists 
-- students age must be > 16
select student_id, first_name, last_name, age, department
from edu.student
where age < 15