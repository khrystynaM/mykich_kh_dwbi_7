use K_M_module_3
go

drop trigger if exists edu.tr_stud_del 
go

---- trigger creation
create trigger edu.tr_stud_del   ON edu.student
AFTER  DELETE
AS
BEGIN

insert into edu.student_2
		([student_id]
		,[last_name]
		,[first_name]
		,[middle_name]
		,[certificate_number]
		,[instituation]
		,[department]
		,[speciality]
		,[age]
		,[year_of_introduction]
		,[phone_number]
		,[type_operation]
		,[date_operation])
select 
		 d.[student_id]
		,d.[last_name]
		,d.[first_name]
		,d.[middle_name]
		,d.[certificate_number]
		,d.[instituation]
		,d.[department]
		,d.[speciality]
		,d.[age]
		,d.[year_of_introduction]
		,d.[phone_number]
		,'delete'
		,getdate()
	FROM deleted d
	

END