use K_M_module_3
go


drop trigger if exists edu.tr_student
go

---- trigger creation
create trigger tr_student ON edu.student
AFTER UPDATE
AS
BEGIN
	update edu.student
	set updated_date = getdate()
	from edu.student
		inner join inserted on edu.student.student_id = inserted.student_id
end
go