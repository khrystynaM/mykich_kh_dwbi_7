use K_M_module_3
go

--- create view (table subject_student)
CREATE OR ALTER VIEW edu.subject_student_view
(	   [subject_id]
	  ,[student_id]
      ,[mark]
      ,[date_exam]
  )
  AS
  SELECT [subject_id]
	  ,[student_id]
      ,[mark]
      ,[date_exam]

  FROM [edu].[subject_student]

go

 -- select data from our view
select *
from edu.subject_student_view