use [K_M_module_3]
go

----insert new record
----insert first record
insert into [edu].[subject_student]
           ([subject_id]
		   ,[student_id]
           ,[mark]
           ,[date_exam])
     values
           (1
           ,1
           ,87
		   ,'2017-09-29')

go

----insert new record
----insert second record
insert into [edu].[subject_student]
           ([subject_id]
		   ,[student_id]
           ,[mark]
           ,[date_exam])
     values
           (1
           ,2
           ,78
		   ,'2017-12-29')
go

----insert new record
----insert third record
insert into [edu].[subject_student]
           ([subject_id]
		   ,[student_id]
           ,[mark]
           ,[date_exam])
     values
           (1
           ,3
           ,50
		   ,'2017-12-27')
go

----insert fourth record
insert into [edu].[subject_student]
           ([subject_id]
		   ,[student_id]
           ,[mark]
           ,[date_exam])
     values
           (2
           ,1
           ,71
		   ,'2017-12-26')
go

----insert fifth record
insert into [edu].[subject_student]
           ([subject_id]
		   ,[student_id]
           ,[mark]
           ,[date_exam])
     values
           (2
           ,3
           ,90
		   ,'2017-11-24')
go

----insert sixth record
insert into [edu].[subject_student]
           ([subject_id]
		   ,[student_id]
           ,[mark]
           ,[date_exam])
     values
           (3
           ,1
           ,84
		   ,'2017-11-18')
go

----insert seventh record
insert into [edu].[subject_student]
           ([subject_id]
		   ,[student_id]
           ,[mark]
           ,[date_exam])
     values
           (3
           ,2
           ,60
		   ,'2017-12-30')
go

----insert eighth record
insert into [edu].[subject_student]
           ([subject_id]
		   ,[student_id]
           ,[mark]
           ,[date_exam])
     values
           (3
           ,3
           ,79
		   ,'2017-12-25')
go

-- select all records from table subject_student
select *
from edu.subject_student
