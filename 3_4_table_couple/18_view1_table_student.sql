use K_M_module_3
go

--- create view (table student)
CREATE OR ALTER VIEW edu.student_view
(
	   [student_id]
      ,[first_name]
      ,[last_name]
      ,[middle_name]
	  ,[phone_number]

  )
  AS
  SELECT [student_id]
      ,[first_name]
      ,[last_name]
      ,[middle_name]
	  ,[phone_number]

  FROM [edu].[student]

go
  -- select data from our view
select *
from edu.student_view