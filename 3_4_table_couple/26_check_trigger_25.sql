use K_M_module_3
go

update edu.student
set last_name = 'Terenchyn'
where last_name = 'Vitoshko'
go

-- check our new table student_2
select *
from edu.student_2
go