use K_M_module_3
go


-- simple select statement table student
select *
from edu.student
go


-- insert student which age < 16 years
-- Error. Age must be > 16
insert into [edu].[student]
           ([student_id]
		    ,[last_name]
           ,[first_name]
           ,[middle_name]
		   ,[certificate_number] 
		   ,[instituation]
		   ,[department]
		   ,[speciality]
		   ,[age]
		   ,[year_of_introduction]
		   ,[phone_number])
     values
           (11
           ,'Yarish'
           ,'Dmytro'
           ,'Ivanovych'
		   ,'KM12645'
		   ,'Computer_science_and_information_technology'
		   ,'Information_systems_and_networks'
		   ,'Computer_science'
		   ,'15'
		   ,'2017-08-30'
		   ,'0972388789')

go


-- simple select statement to check for incorrect data insertion
select *
from edu.student
go