use K_M_module_3
go

-- update data
update [edu].[student]
set age = 19
where student_id = 1

go

-- column "updated_date" must be change 
-- check this

select student_id, age, updated_date
from edu.student