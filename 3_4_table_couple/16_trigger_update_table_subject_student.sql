use K_M_module_3
go


drop trigger if exists edu.tr_subject_student
go

---- trigger creation
create trigger tr_subject_student ON edu.subject_student
AFTER UPDATE
AS
BEGIN
	update edu.subject_student
	set updated_date = getdate()
	from edu.subject_student
		inner join inserted on edu.subject_student.subject_id = inserted.subject_id 
			and edu.subject_student.student_id = inserted.student_id 
			
END
GO