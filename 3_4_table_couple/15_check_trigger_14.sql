use K_M_module_3
go 

-- update data
update [edu].[subject]
set amount_of_hour = 235
where subject_id = 1

go

-- column "updated_date" must be change 
-- check this

select subject_id, amount_of_hour, updated_date
from [edu].[subject]