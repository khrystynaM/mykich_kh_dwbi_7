use K_M_module_3
go


-- simple select statement table student
select *
from edu.student
go


-- insert student with the same phone number
-- Error: [phone_number] is unique key
insert into [edu].[student]
           ([student_id]
		   ,[last_name]
           ,[first_name]
           ,[middle_name]
		   ,[certificate_number] 
		   ,[instituation]
		   ,[department]
		   ,[speciality]
		   ,[age]
		   ,[year_of_introduction]
		   ,[phone_number])
     values
           (12
           ,'Yavdyk'
           ,'Iryna'
           ,'Volodymyrivna'
		   ,'KS12645'
		   ,'Computer_science_and_information_technology'
		   ,'Information_systems_and_networks'
		   ,'Computer_science'
		   ,'18'
		   ,'2017-08-30'
		   ,'0972378789')
go

-- simple select statement to check for incorrect data insertion
select *
from edu.student
go