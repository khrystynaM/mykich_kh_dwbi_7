--- create table 


use K_M_module_3
go

drop table if exists [edu].[subject_student]
go
drop table if exists [edu].[subject]
go
drop table if exists [edu].[student]
go


--- create table 1 - student
create table  [edu].[student] (
       student_id           integer NOT NULL,
	   last_name			varchar(30) NOT NULL,
       first_name           varchar(30) NOT NULL,
	   middle_name			varchar(20) NOT NULL,
	   certificate_number   varchar(20) NOT NULL unique,
	   instituation			varchar(50) NOT NULL,
	   department			varchar(45) NOT NULL,
	   speciality			varchar(45) NOT NULL,
	   age					integer NOT NULL check (age > 16),
	   year_of_introduction	date NOT NULL,
	   phone_number         integer NOT NULL unique,
	   inserted_date		date NOT NULL default (getdate()),
	   updated_date			date NULL, 

	  constraint PK_student primary key(student_id)
	 
) on [Primary]
go

--- create table 2 - subject
create table [edu].[subject] (
       subject_id           integer NOT NULL,
       subject_name         varchar(20) NOT NULL,
	   subject_code         varchar(15) NOT NULL unique,
	   description_subject  varchar(MAX) NOT NULL,
	   teacher_name         varchar(20) NOT NULL,
	   amount_of_hour		integer NOT NULL check (amount_of_hour > 0),
	   type_of_subject		varchar(20) NOT NULL,
	   course_of_studying	integer NOT NULL,
	   year_of_creation		date NOT NULL,
	   inserted_date		date NOT NULL default (getdate()),
	   updated_date			date NULL,  
	  
	   constraint PK_subject primary key(subject_id)	
	  
) on [Primary]
go

--- create table 3 - subject_student
create table [edu].[subject_student](
       subject_id           integer NOT NULL,
	   student_id           integer NOT NULL,
       mark			        integer NOT NULL check (mark >= 0 and mark <= 100),
	   date_exam	        date NOT NULL,
	   inserted_date		date NOT NULL default (getdate()),
	   updated_date			date NULL,  
	  
	   constraint PK_subject_student primary key(subject_id, student_id),	

	   --- create foreign key 
	   constraint FK_student foreign key (student_id) references [edu].[student] (student_id),
	   constraint FK_subject foreign key (subject_id) references [edu].[subject] (subject_id)
) on [Primary]
go
 