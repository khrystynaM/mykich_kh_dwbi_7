use [K_M_module_3]
go

----insert new record
----insert first record
insert into [edu].[student]
           ([student_id]
           ,[last_name]
           ,[first_name]
           ,[middle_name]
		   ,[certificate_number] 
		   ,[instituation]
		   ,[department]
		   ,[speciality]
		   ,[age]
		   ,[year_of_introduction]
		   ,[phone_number])
     values
           (1
           ,'Shvorob'
           ,'Iryna'
           ,'Bogdanivna'
		   ,'KN12345'
		   ,'Computer_science_and_information_technology'
		   ,'Information_systems_and_networks'
		   ,'Computer_science'
		   ,'18'
		   ,'2017-08-30'
		   ,'0972378789')

go

----insert second record
insert into [edu].[student]
           ([student_id]
           ,[last_name]
           ,[first_name]
           ,[middle_name]
		   ,[certificate_number] 
		   ,[instituation]
		   ,[department]
		   ,[speciality]
		   ,[age]
		   ,[year_of_introduction]
		   ,[phone_number])
     values
           (2
           ,'Zavyshak'
           ,'Iryna'
           ,'Ivanivna'
		   ,'KN12346'
		   ,'Computer_science_and_information_technology'
		   ,'Information_systems_and_networks'
		   ,'Computer_science'
		   ,'19'
		   ,'2016-08-30'
		   ,'0934788651')

go
----insert third record
insert into [edu].[student]
           ([student_id]
           ,[last_name]
           ,[first_name]
           ,[middle_name]
		   ,[certificate_number] 
		   ,[instituation]
		   ,[department]
		   ,[speciality]
		   ,[age]
		   ,[year_of_introduction]
		   ,[phone_number])
     values
           (3
           ,'Rybchak'
           ,'Zoryana'
           ,'Lyubomyrivna'
		   ,'KN12347'
		   ,'Computer_science_and_information_technology'
		   ,'Applied_linguistics'
		   ,'Philology'
		   ,'21'
		   ,'2014-08-30'
		   ,'0662365651')

go

----insert fourth record
insert into [edu].[student]
           ([student_id]
           ,[last_name]
           ,[first_name]
           ,[middle_name]
		   ,[certificate_number] 
		   ,[instituation]
		   ,[department]
		   ,[speciality]
		   ,[age]
		   ,[year_of_introduction]
		   ,[phone_number])
     values
           (4
           ,'Vasylyuk'
           ,'Andrij'
           ,'Stepanovych'
		   ,'KN12348'
		   ,'Applied_Mathematics_and_Fundamental_Sciences'
		   ,'Higher_mathematics '
		   ,'Applied_mathematics'
		   ,'21'
		   ,'2014-08-30'
		   ,'0671122651')

go

----insert fifth record
insert into [edu].[student]
           ([student_id]
           ,[last_name]
           ,[first_name]
           ,[middle_name]
		   ,[certificate_number] 
		   ,[instituation]
		   ,[department]
		   ,[speciality]
		   ,[age]
		   ,[year_of_introduction]
		   ,[phone_number])
     values
           (5
           ,'Terenchyn'
           ,'Bogdan'
           ,'Ivanovych'
		   ,'KN12350'
		   ,'Computer_science_and_information_technology'
		   ,'Software'
		   ,'Software_engineering'
		   ,'21'
		   ,'2014-08-30'
		   ,'0932378111')

go


----insert sixth record
insert into [edu].[student]
           ([student_id]
           ,[last_name]
           ,[first_name]
           ,[middle_name]
		   ,[certificate_number] 
		   ,[instituation]
		   ,[department]
		   ,[speciality]
		   ,[age]
		   ,[year_of_introduction]
		   ,[phone_number])
     values
           (6
           ,'Atamanchyk'
           ,'Lesya'
           ,'Vasylivna'
		   ,'KN12351'
		   ,'Chemistry_and_chemical_technology'
		   ,'Pharmacy_and_Biotechnology'
		   ,'Pharmacy'
		   ,'22'
		   ,'2013-08-30'
		   ,'0632578622')

go

----insert seventh record
insert into [edu].[student]
           ([student_id]
           ,[last_name]
           ,[first_name]
           ,[middle_name]
		   ,[certificate_number] 
		   ,[instituation]
		   ,[department]
		   ,[speciality]
		   ,[age]
		   ,[year_of_introduction]
		   ,[phone_number])
     values
           (7
           ,'Matviyiv'
           ,'Lyudmyla'
           ,'Vasylivna'
		   ,'KN12352'
		   ,'Humanities_and_Social_Sciences'
		   ,'Political_science_and_international_relations'
		   ,'International_relations'
		   ,'18'
		   ,'2017-08-30'
		   ,'0672378651')

go

----insert eighth record
insert into [edu].[student]
           ([student_id]
           ,[last_name]
           ,[first_name]
           ,[middle_name]
		   ,[certificate_number] 
		   ,[instituation]
		   ,[department]
		   ,[speciality]
		   ,[age]
		   ,[year_of_introduction]
		   ,[phone_number])
     values
           (8
           ,'Kozak'
           ,'Taras'
           ,'Igorovych'
		   ,'KN12353'
		   ,'Architecture'
		   ,'Architectural_design'
		   ,'Architecture'
		   ,'18'
		   ,'2017-08-30'
		   ,'0676778651')

go

-- select from table all records
select * from [edu].[student]
go